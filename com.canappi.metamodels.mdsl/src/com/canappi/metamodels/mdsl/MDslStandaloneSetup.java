
package com.canappi.metamodels.mdsl;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class MDslStandaloneSetup extends MDslStandaloneSetupGenerated{

	public static void doSetup() {
		new MDslStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

