package com.canappi.metamodels.mdsl.validation;
 
import com.canappi.metamodels.mdsl.mDsl.*;

import org.eclipse.emf.ecore.resource.Resource; 
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.validation.Check;

public class MDslJavaValidator extends AbstractMDslJavaValidator {

//	@Check
//	public void checkGreetingStartsWithCapital(Greeting greeting) {
//		if (!Character.isUpperCase(greeting.getName().charAt(0))) {
//			warning("Name should start with a capital", MyDslPackage.GREETING__NAME);
//		}
//	}
	
	@Check
	public void checkArea(Map m) {
		Double dlat = Double.valueOf(m.getDeltaLat()) ;
		Double dlong = Double.valueOf(m.getDeltaLong()) ;
		
		if (dlat<=0) {
			error("This needs to be a positive number",MDslPackage.MAP__DELTA_LAT) ;
		}
		if (dlong<=0) {
			error("This needs to be a positive number",MDslPackage.MAP__DELTA_LONG) ;
		}
	}

	
	@Check
	public void checkSliderValues(Slider m) {
		Double min = Double.valueOf(m.getMin()) ;
		Double max = Double.valueOf(m.getMax()) ;
		
		if (min>=max) {
			error("The minimum needs to be less than the maximum",MDslPackage.SLIDER__MIN) ;
		}
	}

	@Check
	public void tableWithNoLayout(Table m) {
		
		if (!m.isIsMenu()) {
			if (m.getRowStructure() == null)
				error("A table must either be a menu table or a reference to a layout",MDslPackage.TABLE__ROW_STRUCTURE) ;
		}
		
		
	}
	
	@Check
	public void boundAsArray(Text t) {
		
		if (t.isBindAsArray()) {
			if (t.getLines() == null) {
				error("A textfield can only be bound as an array if it has more than one line",MDslPackage.TEXT__BIND_AS_ARRAY) ;
			}
		}
	}

	@Check
	public void checkMain(Main o) {
	
		String sscreen = o.getSplashScreen();
		if (sscreen != null) {
			warning("Splashscreen is deprecated and need to be set in the iOS or Android project directly", MDslPackage.MAIN__SPLASH_SCREEN);
		} 
	
	}

	@Check
	public void checkOpenBrowser(Action o) {
		
		Object url = o.getUrl() ;
		if (url != null) {
			if (url.getClass().equals(Text.class)) {
				warning("Url is a Text", MDslPackage.ACTION__URL);
			} else {
				warning("Url is a QualifiedName", MDslPackage.ACTION__URL);
				EList<Diagnostic> errors = o.eResource().getErrors() ;
				errors.clear() ;
			}
		} 
	}

	
	@Check
	public void roundedCorderForGradientButton(Button m) {
		GradientStyle gs = m.getGradientStyle() ; 
		RoundedCorners rc = m.getRoundedCorners() ;
		
		ButtonStyle s = m.getStyle() ;
		
		
		if (s.getValue() ==  ButtonStyle.GRADIENT_VALUE) {
			if (rc != null)
				error("A Gradient Button cannot have rounded corners",MDslPackage.BUTTON__ROUNDED_CORNERS) ;
		} else { 
			if (gs.getValue()>0) {
				error("You cannot specify a gradient type for a non gradient button",MDslPackage.BUTTON__GRADIENT_STYLE) ;
			}
		}
		
		if (m.isIsLeft()) {
			if (s.getValue() != ButtonStyle.NAVBAR_VALUE) {
				error("You can only select the left option for a Navbar button",MDslPackage.BUTTON__IS_LEFT) ;
			}
		}
	}

	
	@Check
	public void operationConsistency(Operation o) {

		if ((o.getProtocol() == Protocol.TCP))
		{
			if (o.getVerb() != VERB.get("COMMAND")) {
				error("TCP requires the verb COMMAND",MDslPackage.OPERATION__VERB) ;
				
			}
			if (o.getHost() == null) {
				error("TCP requires a host value",MDslPackage.OPERATION__VERB) ;
			}
		}
		
		if (o.isIsJson() && (o.getProtocol() == Protocol.SOAP))
		{
			error("JSON format is not allowed for SOAP protocol",MDslPackage.OPERATION__IS_JSON) ;
		}
		
		if (o.getProtocol() == Protocol.SOAP) {
			if ((o.getVersion() != ProtocolVersion.V11) && (o.getVersion() != ProtocolVersion.V12)) {
				error("SOAP versions are soap11 or soap12",MDslPackage.OPERATION__VERSION) ;
			}
		}
		if (o.getProtocol() == Protocol.RSS) {
			if ((o.getVersion() != ProtocolVersion.V1) && (o.getVersion() != ProtocolVersion.V2)) {
				error("RSS versions are rss1 or rss2",MDslPackage.OPERATION__VERSION) ;
			}
		}
	}
	
	@Check
	public void checkConnection(Connection o) {

		EList<Operation> operations = o.getOperations() ;
		boolean isInit = false ;
		for(Operation op : operations) {
			if (op.isIsInit()) {
				if (isInit) {
					error("This connection "+o.getName()+" has more than one operation with the init parameter set"+op.getName(),MDslPackage.CONNECTION__OPERATIONS) ;			
				} else {
					isInit = true ;
				}
			}
		}
	}

	@Check
	public void checkInvokeAction(Action o) {

		Connection c = o.getConnection() ;
		if (c != null)
		if (o.getOpName() != null)
		{
			boolean found = false ; 
			EList<Operation> ops = c.getOperations();
			for ( int i = 0 ; i < ops.size() ; i++ ) {
				Operation op = ops.get(i) ;
				if (op.getName().equals(o.getOpName())) found = true ;
			}
			if (!found) {
				error("The operation "+o.getOpName()+" is not defined in the connection "+c.getName(),MDslPackage.ACTION__CONNECTION) ;
			}
		}
	}
	
	@Check
	public void checkHTMLExtension(Browser o) {

		String fileName = o.getFile() ;
		if (fileName != null)
		{
			if (fileName.indexOf('.')>=0) {
				error("The file name cannot have an extension, for 'myfile.html', enter simply 'myfile'",MDslPackage.BROWSER__FILE) ;
			}
		}
	}

	@Check
	public void checkPlayerContent(MoviePlayer o) {

		
		if (o.getFile() == null)
		{
			if (o.getUrl() == null) {
				error("You need to enter either a file or a URL",MDslPackage.MOVIE_PLAYER__FILE) ;
			}
		}
		
		if (o.getInSeconds() != null) {
			warning("Delayed play is not implemented",MDslPackage.MOVIE_PLAYER__IN_SECONDS) ;			
		}
	}

	@Check
	public void checkImageBorderAction(Image o) {

		
		if (o.getAction() != null)
		{
			if (o.getBorderSize() != null) {
				warning("Borders are not displayed when an action is associated to the image",MDslPackage.IMAGE__BORDER_SIZE) ;
			}
		}
	}

	/*
	@Check
	public void checkIntType(IntType number) {
	
		String u = number.toString() ;
		
		warning("This is an int:"+u, MDslPackage.INT_TYPE ) ;
	}
*/
}
