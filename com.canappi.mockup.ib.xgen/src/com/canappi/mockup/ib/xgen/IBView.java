package com.canappi.mockup.ib.xgen;

import java.util.ArrayList;
import java.util.ListIterator;

public class IBView implements IB {

	protected ArrayList<IBObject> controls  ;
	
	public IBView() {
		controls = new ArrayList<IBObject>() ;
	}
	
	public void addControl(IBObject control) {
		controls.add(control) ;
	}

	
	public String serialize(String target) {
		StringBuffer view = new StringBuffer();
		ListIterator<IBObject> iter = controls.listIterator() ;
		for (IBObject c = iter.next() ; iter.hasNext() ; iter.next() ) {
			view.append(c.serialize(target)) ;
		}
		return view.toString() ;
	}
	
}
