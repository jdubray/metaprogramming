package com.canappi.mockup.ib.xgen;

public interface IB {

	static String _mdsl = "mdsl";

	static String _archive = "archive";
	static String _data = "data";
	static String _object = "object";
	static String _class = "class";
	static String _key = "key";
	static String _array = "array";
	static String _integer = "int";
	static String _string = "string";
	static String _bool = "bool";
	static String _reference = "reference";
	static String _nil = "nil";
	static String _bytes = "bytes";
	static String _boolean = "boolean";
	static String _real = "real";
	static String _float = "float";
	static String _value = "value";

	static String _IBUIView = "IBUIView";
	static String _NSMutableArray = "NSMutableArray";
	static String _NSSubviews = "NSSubviews";
	static String _NSFrame = "NSFrame";
	static String _IBUIText = "IBUIText" ;
	static String _IBUILabel = "IBUILabel" ;
	static String _NSFont = "NSFont" ;
	static String _NSSize = "NSSize" ;
	static String _IBUIButton = "IBUIButton" ; 
	static String _IBUIOn = "IBUIOn" ;
	static String _IBUINormalTitle = "IBUINormalTitle";
	static String _IBUISelectedTitle = "IBUISelectedTitle" ;
	static String _NSCustomResource = "NSCustomResource";
	static String _IBUINormalImage = "IBUINormalImage" ;
	static String _IBUISelectedImage = "IBUISelectedImage" ;
	
	static String _NSColor = "NSColor";
	static String _NSRGB ="NSRGB" ;
	
	static String _IBUISegmentedControl ="IBUISegmentedControl";
	static String _IBNumberOfSegments ="IBNumberOfSegments";
	static String _IBSegmentTitles ="IBSegmentTitles";
	static String _IBSegmentWidths ="IBSegmentWidths";

	static String _IBUITextField ="IBUITextField";
	static String _IBUITextColor ="IBUITextColor";
	static String _IBUIPlaceholder = "IBUIPlaceholder";
	static String _IBUISecureTextEntry = "IBUISecureTextEntry";
	static String _IBUIClearButtonMode = "IBUIClearButtonMode";
	
	static String _IBUISlider ="IBUISlider";
	static String _IBUIValue ="IBUIValue";
	
	static String _IBUISwitch ="IBUISwitch" ; 
	static String _IBUIContentHorizontalAlignment ="IBUIContentHorizontalAlignment" ; 
	static String _IBUIContentVerticalAlignment ="IBUIContentVerticalAlignment" ; 
	
	static String _IBUIActivityIndicatorView ="IBUIActivityIndicatorView" ; 
	static String _IBUIHidesWhenStopped ="IBUIHidesWhenStopped" ; 
	static String _IBUIStyle ="IBUIStyle" ; 
	static String _IBUIOpaque ="IBUIOpaque" ; 

	static String _IBUIProgressView ="IBUIProgressView" ; 
	static String _IBUIProgress ="IBUIProgress" ; 

	static String _IBUIImageView ="IBUIImageView" ; 
	static String _NSResourceName ="NSResourceName" ; 
	static String _NSClassName ="NSClassName" ; 
	static String _NSImage ="NSImage" ; 

	static String _IBADBannerView ="IBADBannerView" ; 
	static String _DBannerContentSize320x50 ="DBannerContentSize320x50" ; 
	static String _ADBannerContentSize480x32 ="ADBannerContentSize480x32" ; 

}
