package com.moppr.balsamiq.xgen;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Xgen implements BMML {

	/**
	 * @param args
	 */
	static ArrayList<String> layouts ; 
	static ArrayList<String> views ; 
	static String main ;
	
	public static String replaceAllHex(String s) {
	
		StringBuffer r = new StringBuffer("");
		
		while (s.indexOf("%")>=0) {
			int p = s.indexOf("%") ;
			System.out.println(s+"-->"+r+"<"+String.valueOf(p)+">"+s.substring(p+1,p+3) ) ;
			int i = Integer.parseInt(s.substring(p+1,p+3),16) ;
			if (p>0) r.append(s.substring(0, p)) ;
			char c = (char)i;
			r.append(c);
			s = s.substring(p+3, s.length());
		}
		r.append(s) ;
		return r.toString();
	}
	
	public final static String resourcify(String source) {
		String replacements[] = {"!", "*", "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]", "  "};
		for (int i = 0 ; i < replacements.length; i++) 
		{
			source = source.replace(replacements[i], "") ;
		}
		source = source.replace(" ", "_") ;
		source = source.replace(".", "") ;
		if (source.lastIndexOf("_") ==  source.length()-1) return source.substring(0, source.length()-1) ;
		return source ;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
		try {
			  File file = new File(args[0]);
			  FileWriter outFile = new FileWriter(args[1]);
			  PrintWriter out = new PrintWriter(outFile);
							
			  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			  DocumentBuilder db = dbf.newDocumentBuilder();
			  Document doc = db.parse(file);
			  doc.getDocumentElement().normalize();
			  System.out.println("Root element " + doc.getDocumentElement().getNodeName());
			  NodeList nodeLst = doc.getElementsByTagName(_groups);
			  System.out.println("Found "+nodeLst.getLength()+" Views");
			  
			  views = new ArrayList<String>();
			  layouts = new ArrayList<String>();

			  for (int s = 0; s < nodeLst.getLength(); s++) {
				//get all views; 
				Element viewElement = (Element)nodeLst.item(s);
				String viewDec = "" ;
				if (s == 0) {
					main = "\nmain application {\n"

						+  "     navigationBar ;\n"
						+  "     start view0 ;\n"
						+  "     menu { view0\n" ; 
						
				} else {
					main += ", view"+String.valueOf(s);
				}
				NodeList controls = viewElement.getElementsByTagName(_control);
				viewDec = "\nview view"+String.valueOf(s)+ " 'View Title' {\n"
	
						+ "	    color groupTableViewBackground ;\n"
						+ "     controls stacked {\n"
						+ "          layout view"+String.valueOf(s)+ "Layout  ;\n"
						+ "     }\n}\n";
				//get all controls
				String layoutDec = "layout view"+String.valueOf(s)+ "Layout stacked { \n";
				System.out.println("Found "+String.valueOf(controls.getLength())+" controls in view "+String.valueOf(s));
				
				String selectedText = "" ;
				for (int c = 0; c < controls.getLength(); c++) {
					
					Element control = (Element)controls.item(c);
			        if (control != null)
				    if (control.getNodeType() == Node.ELEMENT_NODE) {
				      
				      NamedNodeMap attrs = control.getAttributes();
				      String controlId = attrs.getNamedItem(_controlId).getNodeValue();
				      String x = attrs.getNamedItem(_x).getNodeValue();
				      String y = attrs.getNamedItem(_y).getNodeValue();
				      String width = attrs.getNamedItem(_width).getNodeValue();
				      String height = attrs.getNamedItem(_height).getNodeValue();
				      //Get other control properties
				      NodeList n = control.getElementsByTagName("controlProperties") ;
				      System.out.println(control.toString());
				      if (n.item(0) != null) {
				    	  
					  NodeList properties = n.item(0).getChildNodes();
					  String text = "";
					  String src = "" ;
					  String selectedIndex = "" ;
				      System.out.println(controlId+" "+n.item(0).getNodeName()+"has "+properties.getLength()+" props") ;
				      for( int p = 0 ; p < properties.getLength(); p++) {
				    	  Node property = properties.item(p);
				    	  if (property != null) {
				    		  
				    	  System.out.println(">>"+property.getNodeName()+" /"+property.getTextContent()+"/" );
				    	  if (property.getNodeName().equals(_text)) text = property.getTextContent();
				    	  if (property.getNodeName().equals(_src)) src = property.getTextContent();
				    	  if (property.getNodeName().equals(_selectedIndex)) { 
				    		  selectedIndex = property.getTextContent();
				    	  }
				    		  }
				      }
				      text = replaceAllHex(text) ;
				      src = replaceAllHex(src);
				      if (!selectedIndex.equals("")) selectedText = selectedText(text,selectedIndex,",") ;
				      System.out.println(text);
				      System.out.println(src);
				      if (text == null) text = "";
				      if (src == null) src = "";
				      if (text.equals("")) text = "control"+String.valueOf(c);
				      String controlName = resourcify(text) ;
				      if (controlId.equals(_label)) {
				    	  if (text.length() < 60) { 
				    		  layoutDec += "\tlabel "+controlName+"Label '"+text+"' ("+x+","+y+","+width+","+height+") {Left;}\n" ;
				    	  } else { 
				    		  layoutDec += "\tweb "+controlName+"RichText ("+x+","+y+","+width+","+height+") {\n";
				    	      layoutDec += "\t\tdisplay '"+text+"' ;" ;
				    	      layoutDec += "\t}" ;
				      	}
				      }
				      if (controlId.equals(_textField)) {
				    	  layoutDec += "\ttext "+controlName+" '"+text+"' ("+x+","+y+","+width+","+height+") {Left; border bezel;}\n" ;
				      }
				      if (controlId.equals(_button) | controlId.equals(_pointyButton)) {
				    	  layoutDec += "\tbutton "+controlName+" '"+text+"' ("+x+","+y+","+width+","+height+")  { action "+controlName+"Action ;   }\n" ;
				      }
				      if (controlId.equals(_image)) {
				    	  layoutDec += "\timage "+controlName+"Image '"+text+"' ("+x+","+y+","+width+","+height+") ";
				    	  if (src.equals("")) layoutDec +=";\n" ;
				    	  else layoutDec += "{ '"+src+"' } \n";
				      }
				      if (controlId.equals(_switch)) {
				    	  layoutDec += "\tswitch "+controlName+"Switch ("+x+","+y+","+width+","+height+") \n" ;
				      }
				      if (controlId.equals(_icon)) {
				    	  layoutDec += "\timage "+controlName+"Image ("+x+","+y+","+width+","+height+") \n" ;
				    	  if (!src.equals("")) layoutDec += "{ '"+src+"' ; }";
				      }
				      
				      if (controlId.equals(_checkBox)) {
				    	  layoutDec += "\tcheckbox "+controlName+"CheckBox ("+x+","+y+","+width+","+height+") {\n" ;
				    	  layoutDec += "\t\t label '"+text+"' ; }";
				      }
				      if (controlId.equals(_radioButton)) {
				    	  layoutDec += "\tradiobutton "+controlName+"RadioButton ("+x+","+y+","+width+","+height+") {\n" ;
				    	  layoutDec += "\t\t label '"+text+"' ; }";
				      }
				      if (controlId.equals(_radioButtonGroup)) {
				    	  layoutDec += "\tradiobutton "+controlName+"RadioButton ("+x+","+y+","+width+","+height+") {\n" ;
				    	  layoutDec += "\t\t label '"+text+"' ; group ; }";
				      }
				      if (controlId.equals(_searchBox)) {
				    	  layoutDec += "\t//searchbox "+controlName+"SearchBox ("+x+","+y+","+width+","+height+") { }\n" ;
				      }
				      if (controlId.equals(_slider) | controlId.equals(_slider2) | controlId.equals(_slider3)) {
				    	  layoutDec += "\tslider "+controlName+"Slider ("+x+","+y+","+width+","+height+") {\n" ;
				    	  layoutDec += "\t\t min 0 ; max 10 ; step 1 ; initialValue 0; continuous; }";
				      }
				      if (controlId.equals(_table)) {
				    	  layoutDec += "\ttable "+controlName+"Table ("+x+","+y+","+width+","+height+") {\n" ;
				    	  layoutDec += "\t\t header '"+text+"' ; ";
				      }
				      if (controlId.equals(_web)) {
				    	  layoutDec += "\tweb "+controlName+"Browser ("+x+","+y+","+width+","+height+") {\n" ;
				    	  layoutDec += "\t\t url = '"+text+"' }";
				      }
				      if (controlId.equals(_map)) {
				    	  layoutDec += "\tmap "+controlName+"Map ("+x+","+y+","+width+","+height+") { \n" ;
				      }
				      if (controlId.equals(_iconAndLabel)) {
				    	  layoutDec += "\timage "+controlName+"Image ("+x+","+y+","+width+","+height+") \n" ;
				    	  if (!src.equals("")) layoutDec += "{ '"+src+"' ; }";
				    	  if (!text.equals("")) {
				    		  //TODO to calculate the right coordinates
					    	  layoutDec += "\tlabel "+controlName+"Label '"+text+"' ("+x+","+String.valueOf(Integer.valueOf(y)+Integer.valueOf(height)+15)+","+width+","+30+") {Center;}\n" ;				    	  
				    	  }
				      }
				      if (controlId.equals(_browser)) {
				    	  layoutDec += "\tweb "+controlName+"Browser ("+x+","+y+","+width+","+height+") {\n" ;
				    	  if (!text.equals("")) {
				    		  
					    	  layoutDec += "\tstart '"+text+"' ;\n" ;				    	  
				    	  }
				    	  
				      }
				      if (controlId.equals(_canvas)) {
				      }
				      if (controlId.equals(_datePicker)) {
				    	  layoutDec += "\tdatePicker "+controlName+"DatePicker ("+x+","+y+","+width+","+height+") ;\n" ;			    	  
				      }
				      if (controlId.equals(_link)) {
				    	  
				      }
				      if (controlId.equals(_tableMenu)) {
				    	  
				      }
				      if (controlId.equals(_paragraph)) {
					      if (controlId.equals(_textField)) {
					    	  layoutDec += "text "+controlName+" '"+text+"' ("+x+","+y+","+width+","+height+") {Left; border bezel;}\n" ;
					      }
				    	  
				      }

				      }
				    }
				}
				layoutDec += "}\n";
				layouts.add(layoutDec);
				
				if (!selectedText.equals("")) {
					viewDec = viewDec.replaceAll("View Title", selectedText) ;
				}
				views.add(viewDec);


			  }
			  if (args[2] != null) out.println("package "+args[2]+";\n"); else out.println("package com.yourcompany.yourapp ;\n");
			  for (int l = 0 ; l < layouts.size(); l++) {
				  out.println(layouts.get(l)) ;
			  }
			  for (int v = 0 ; v < views.size(); v++) {
				  out.println(views.get(v)) ;
			  }
			  main += "}\n}" ;
			  out.println(main) ;
			  out.close();

			  } catch (Exception e) {
			    e.printStackTrace();
			  }	}

	private static String selectedText(String text, String selectedIndex, String separator) {
		String selectedText = "" ;
		Integer s = Integer.valueOf(selectedIndex) ;
		Integer b = text.indexOf(separator, s)+1 ;
		Integer e = text.indexOf(separator, s+1) ;
		if (e <= 0 ) e = text.length() ;
		selectedText = text.substring(b, e) ;
		return selectedText ;
	}

}
