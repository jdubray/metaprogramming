grammar com.canappi.metamodels.mdsl.MDsl with org.eclipse.xtext.common.Terminals

generate mDsl "http://www.canappi.com/metamodels/mdsl/MDsl"

import 'http://www.eclipse.org/emf/2002/Ecore' as ecore

Model:
    (package=Package)
	(imports+=Import)*
    (connections+=Connection)* 
    (mappings+=Mapping)*
    (entities+=DataType)*
    //(performers+=Performer)*
    (styles+=Style)*
    (controls+=Spacer)*
	(views+=View)*
	//(processes+=Process)*
	(mains=Main)
	;


Import :
	'import' importURI=STRING;

Package:
	'package' package=QualifiedName ";"; 


//******************************************************************************
// Data Types
//
//******************************************************************************
	

terminal INT returns ecore::EInt:('0'..'9')+ ; 

FLOAT returns ecore::EString : '-'? INT ('.' INT)? ;

POSINTNUMBER returns ecore::EString  : INT  ;

NEGINTNUMBER returns ecore::EString  : '-' INT ;

INTNUMBER :  POSINTNUMBER | NEGINTNUMBER ;

NATIVE:
	STRING | FLOAT 
; 

enum NativeTypes:
	bit | int | float | string | date | time | timestamp | uri | blob | guid | reference | currency
;

enum NativeTypeQualifiers:
	tiny | small | medium | big | unsigned
;

DataType:
	 NativeType | Entity | Association ;

enum Encoding:
	western | i18n | ascii ;

enum Collation:
	latin ;

NativeType:
	'native' name=ID 'as' qual=NativeTypeQualifiers pType = NativeTypes ('[' size=POSINTNUMBER ']')? (encoding=Encoding)? (collation=Collation)? ';'
;

Property:
	type=[DataType] (atLeastOne?='+')? name=ID ('(' defaultValue=STRING ')')? (',' isListBy?='List' ('by')? )? (',' isJoined?='join' )?   (',' isEncrypted?='encrypted')? (',' isSilent?='silent')?  ('as' isEnum?='enum' '{' (values+=STRING ((',') values+=STRING)*) '}')? ('@' description=STRING)? ';'
;

Entity:
	'entity' name=ID (hasLogin?='login')? '{'
		('@' description=STRING)?
		(properties+=Property)+
	'}'	
	 	
;	


enum AssociationType:
	one2many | one2one  | many2many  // | Many2One2Many ='N-1-M'  | Many2One2One = 'N-1-1'  
;

enum Operator:
	equal='==' | lt='<' | gt='>'  | lte='<='  | gte='>=' | notEqual='!='  
;

Association: 
	'association' associationType=AssociationType name=ID  '{'  // ('<' '>')?
		('@' description=STRING)?
		'(' entity1=[Entity] ',' (atLeastOne?='+')? entity2=[Entity] (',' entity3=[Entity])? ')' ('where' constraint=[Property] operation=Operator value=STRING)?  ';'
		(properties+=Property)*
		'}' 
;

enum Templates:
	control | import2 | definition | release | picker | table | handleSection | userDefault | delegate | synthesize | actionRule
;
	

//******************************************************************************
// Misc Types
//
//******************************************************************************

QualifiedName returns ecore::EString : ID ('.' ID)*;
	
StringArray:
	'[' (values+=STRING ((',') values+=STRING)*) ']'
	;
	
IntArray:
	'[' values+=INTNUMBER (',' values+=INTNUMBER)* ']'
	;


SimpleType:
	type=STRING;
	
SimpleVariable:
	variable=STRING;

SimpleOperation:
	operation=STRING;

ArrayType:
	UnboundArray | BoundArray
;

BoundArray:
	(isBound?='[0..' size=POSINTNUMBER ']')
;

UnboundArray:
 (isUnbound?='[]') 
;

	
VARID:
	ID; 

EmptyArray:
	isEmpty?='{}' ;

EmptyCollection:
	isEmpty?='[]' ;

ElementCollection:
	'['  (items+=Element ((',') items+=Element)*) ']' ;

ElementValue:
	value=STRING | ElementCollection | EmptyArray | EmptyCollection ;

Element: 
	'{' element=STRING ':'  (content = ElementValue) '}';

DataSource:
	root = Element ;


KeyValuePair:
	'{' key=STRING ':' value=STRING '}'  
;

KeyValue:
	 key=STRING ':' value=STRING  
;

//******************************************************************************
// Controls
//
//******************************************************************************

Control:
	Label | Table | Gallery | Graph   | Alert | ActionSheet | Picker | DatePicker  | Text | Button | Image | 
	Slider | Segment | Spacer | Switch  | Map | Browser | CheckBox | RadioButton  | MoviePlayer | BarcodeScanner | AdBanner
;	

Frame:
	frame?='(' (x=INTNUMBER)?','(y=INTNUMBER)?','(width=POSINTNUMBER)?','(height=POSINTNUMBER)? (large?='+' (x2=INTNUMBER)?','(y2=INTNUMBER)?','(width2=INTNUMBER)?','(height2=INTNUMBER)?)?')'	
;

FrameImage:
	frame?='(' (x=INTNUMBER)?','(y=INTNUMBER)? (hasWidth?=','(width=POSINTNUMBER)?','(height=POSINTNUMBER)?)? (large?='+' (x2=INTNUMBER)?','(y2=INTNUMBER)? (hasWidth2?=','(width2=POSINTNUMBER)?','(height2=POSINTNUMBER)?)?)?')'	
;

RoundedCorners:
	isRounded?='roundedCorner' ('('radius=FLOAT')')?
;

enum SpacerType:
	sideBySide | stacked | flow | grid
;

enum PinType:
	RedPins | GreenPins | PurplePins
;

enum AudioQuality:
	min | low | medium | high | max
;

ActionParameter:
	name=VARID ('as' mappedTo=VARID)?
;

enum PlayerAction:
    start | pause | resume | stop | record
;


Performer:
	'performer' (isGroup?='group')? (isRole?='role') (isUser?='user')?  performername = ID    '{' (performervalues+=KeyValuePair (',' performervalues+=KeyValuePair)*) '}'  ';' 
;


Action:
 'action' action=ID (':')? 
 						('@' description=STRING )? 
  						('navigate' (isRoot?='top')? (isBack?='back')? ('to' view=[View] ('with'params+=ActionParameter (','params+=ActionParameter)*)?)?   )? 
                        ('shareInformation' informationToShare=[Text] (shareUrl=STRING)?  (imageToShare=[Image])?)? ('on' (twitter?='twitter')? ((',')?facebook?='facebook')? ((',')? shareEmail?='email')? )?
                        ('media' player=[MoviePlayer] playerAction=PlayerAction)?
      					(('update')? isUpdateDefaults?='defaults')? 
 						('sendEmail' (emailAddress=[Text])? (staticEmailAddress=STRING ('as' emailUserDefault?='default')?)?)?
 						('openBrowser' (staticUrl=STRING ('as' urlUserDefault?='default')?)? (url=[Text|QualifiedName])?)?
 						('phoneSomeone' (phone=[Text])? (staticPhone=STRING ('as' phoneUserDefault?='default')?)? (';')?)?
 						('addEventToCalendar' 'start' startDate=[DatePicker] ('end' endDate=[DatePicker])? ('title' title=[Text])? ( ',' staticTitle=STRING)?)?
 						('invokeOperation' connection=[Connection] ('.')? opName=VARID ('as' isCRUD=CRUD)? ('catch' exceptions+=Exception)*)?
 						//('invokeProcess' process=[Process])?
 						('showDirection' toAddress=[Text] ('+'fields+=[Text])* (('from')? currentLocation?='current' ('location')?)? (fromAddress=[Text])?)?
 						//('complete' activity=[Activity])?
 						(dismiss?='dismiss')?
 						('present' presentView=[View] ('for' forSeconds=POSINTNUMBER ('seconds')?)?)?
 						('showAlert' dialogTitle=STRING ('and' message=STRING)? ('('rightButton=STRING (',' leftButton=STRING  )? ')')?)?
 						(isClearForm?='clearForm' ('for' (formParams+=ActionParameter ((',') formParams+=ActionParameter)*))? (';')?)?
 						('store' s3Image=[Image] ('in' 'S3')? 'bucket' bucketRef=[Text] 'filename'nameRef=[Text] (isAuto?='generated')?)?
 						(updateView?='updateView')?
 						('invokeMethod' userexit=VARID)? 
 						';'  
 						 
;

Exception:
	var=[Text] operator=Operator nativeType=NativeTypes '(' value=NATIVE ')' '{' //was NATIVE
		action = Action 
	'}'
;

Spacer:
	'layout' name=VARID (layoutType=SpacerType)? '{' ('@' description=STRING)? (readOnly?='readOnly')?
	(controls+=Control)* 
	('tablet' tabletLayout=[Spacer] ';')?
	('down' downLayout=[Spacer] ';')?
	('left' leftLayout=[Spacer] ';')?
	('right' rightLayout=[Spacer] ';')?
	('catch' '{' (exceptions+=Exception)* '}')? 
	 '}' (';')? 	
;

Segment:

	'segment' name=VARID (frame=Frame)?  ('{'
	
		('@' description=STRING ';')? 
		(values=StringArray)
		(action=Action)?
	
	'}')? (';')?
;

AdBanner:
	'banner' name=VARID (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
		(isIAd?='iAd' ';')?

		(isAdMob?='AdMob' ';')?
	
	'}')?
;

enum MapType:
	Standard | Satellite | Hybrid
;

Map:

	'map' name=VARID (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
		(type=MapType ';')?
		(showUser ?='show' ('user')? ';')?
		(followUser ?='follow' ('user')? ';')?
		(action=Action)?
		('area' deltaLat=FLOAT ',' deltaLong = FLOAT ';')?
		('location' locLat=FLOAT ',' locLong = FLOAT ('(' locName= STRING ',' locAddress= STRING ')')? ';')?
		('locationReference' locLatRef=[Text] ',' locLongRef = [Text] ('(' locNameRef = [Text] ',' locAddressRef = [Text] ')')? ';')?
		//(geoCoder?='geocoder' ';')?
	'}')? (';')?
;


Browser:
	'web' name=VARID (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
		('start' url=STRING ';')?
		('load' file=STRING ';')?
		('display' page=STRING ';')?
		('backgroundColor' bckColor=Color ';')?
	'}')? (';')?
;

CheckBox:
	'checkbox' name=VARID (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
		'label' label=STRING ';' (checked?='selected' ';')? ('color' color=Color ';')?
	'}')? (';')?
;

RadioButton:
	'radiobutton' name=VARID ('as' isStar?='star')? ('(' value=NATIVE ')')? (frame=Frame)?  ('{' //WAS NATIVE
		('@' description=STRING ';')? 
        ('group' groupName=VARID ';')?
		'label' label=STRING ';' (selected?='selected' ';')? ('color' color=Color ';')?
	'}')? (';')?
;

Switch:

	'switch' name=VARID (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
		(defaultValue ?= 'on' ';')?
		('color' color = Color ';')?
		(action=Action)?
	'}')? (';')?
;

Slider:
	'slider' name=VARID  (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
		('min' min=FLOAT ';')? 
		('max' max=FLOAT ';')?
		('step' step=FLOAT ';')?
		('initialValue' init=FLOAT ';')?
		('leftImage' leftImage=STRING ';')?
		('rightImage' rightImage=STRING ';')?
		('thumbImage' thumbImage=STRING ';')?
		('trackColor' trackColor=Color ';')?
		('thumbColor' thumbColor=Color ';')?
		('background' bckColor=Color ';')?
		//('minImage' minImage=STRING ';')?
		//('maxImage' maxImage=STRING ';')?
		(action=Action)?
		(continuous?='continuous' ';')?
		'}')? (';')?
;

enum Colors:
	clear | white | brown | cyan | gray | darkGray | blue | black | darkText | lightGray | lightText | 
	magenta | green | orange | purple | red | yellow | groupTableViewBackground | scrollViewTexturedBackground | viewFlipsideBackground
;
	
enum TextBorder:
	none | line | bezel | rounded
;
	



enum ImageMode:
	Never | WhileEditing | UnlessEditing | Always
;

enum TextEntry: 
	textEntry | numberEntry | emailEntry | urlEntry | phoneEntry | decimalEntry 
	
;

Style:
	'style' name=VARID ('{'  //(additions+=Addition)? for binding
		('font' font=STRING ';')? 
		('size' size= POSINTNUMBER ';')? 
		('color' color=Color ';')? 
		('backgroundColor' backgroundColor=Color ';')?	
		//(isBold?='bold' ';')? 
		//(isItalic?='italic' ';')? 
		//(hasShawdow?='shadow' '('offsetX=FLOAT','offsetY=FLOAT')' ';')? 
		(alignment=Alignment ';')? 
		(hasClearButton?='clearButton' (clearButtonMode=ImageMode)? ';')? 
		//(isAutocorrected?='autocorrect' ';')? 
		(isAutocomplete?='autocomplete' ';')? 
		//(isNumeric?='numeric' ';')? 
		//(hasDetectors?='detectors' ';')? 
		(roundedCorners=RoundedCorners ';')? 
		(orientation=KeyboardSideLC button=Button (buttonMode=ImageMode)? )? 
		('border' border=TextBorder ';')? 
		//('backgroundImage' backgroundImage = STRING ';')?		
	'}')?
;

Text:
	'text' name=VARID (defaultText=STRING)? (('as')? isDate?='date')? (frame=Frame)?   ('{' 
		('@' description=STRING ';')? 
		(('with')? 'style' style=[Style] ';')?  //(additions+=Addition)? for binding
		('font' font=STRING ';')? 
		('size' size= POSINTNUMBER ';')? 
		('color' color=Color ';')? 
		('backgroundColor' backgroundColor=Color ';')?	
		//(isBold?='bold' ';')? 
		//(isItalic?='italic' ';')? 
		//(hasShawdow?='shadow' '('offsetX=FLOAT','offsetY=FLOAT')' ';')? 
		(alignment=Alignment ';')? 
		(readOnly?='readOnly' ';')? 
		(hasClearButton?='clearButton' (clearButtonMode=ImageMode)? ';')? 
		(isSecure?='secure' ';')? 
		('entry' textEntry=TextEntry ';')?
		//(isAutocorrected?='autocorrect' ';')? 
		(isAutocomplete?='autocomplete' ';')? 
		//(isNumeric?='numeric' ';')? 
		//(hasDetectors?='detectors' ';')? 
		('lines' lines=POSINTNUMBER ';')? 
		('placeholder' placeHolder=STRING ';')? 
		(roundedCorners=RoundedCorners ';')? 
		(orientation=KeyboardSideLC button=Button (buttonMode=ImageMode)? )? 
		('border' border=TextBorder ';')? 
		//('backgroundImage' backgroundImage = STRING ';')?	
		(isUnbound?='without' (bindings?='bindings')? ';')?	
		(('bind')? ('as')? bindAsArray?='array' ';')?	
		('format' format=STRING ';')?
		('guard' performer=[Performer]  'binding' guardValue=[Text] ';')? //(cantEdit?='edit')?
	'}')? (';')?
	
;	



enum TableStyle:
	Plain | Grouped ;

enum TableSeparatorStyle:
	Line | LineEtched ;

enum SelectionStyle: Blue | Gray ;

MenuItem: 
	'menuitem' item=STRING '{' action=Action ('icon' iconLeft=STRING ';')? ('button' iconRight=STRING ';')?  '}'
;

enum TableCellStyle:
	Default | Value1 | Value2 | Subtitle
;

enum MenuItemStandardButton:
	none | indicator | detail | checkmark 
; 

Table:
	'table' name=VARID (('as')? isMenu?='menu')? (('as')? processList?='process' ('list')? 	)? (frame=Frame)?  '{' 
		('@' description=STRING ';')? 
		//(isSearchable?='searchable' (refreshPeriod=POSINTNUMBER)? ';')?
		(isSelectable?='selectable' (multiple?='multiple')? key=STRING ';')?
		//(isCategorizable?='category' category=VARID ';')?
		//(readOnly?='readOnly' ';')?
		(rowStructure=[Spacer] arrayType=ArrayType ';')?
		(isProcessList?='process' ('list')? ';')?
		//(roundedCorners=RoundedCorners ';')?		
		('rowHeight' rowHeight=POSINTNUMBER ';')?
		//(editable?='editable' ';')? 
		(action=Action)?
		//(style=TableStyle ';')?
		//(separatorStyle=TableSeparatorStyle ';')?
		//('separator' separatorColor=Color ';')?
		('background' (backgroundColor=Color)? (backgroundImage=STRING)? ';')?
		('selection' selectionStyle=SelectionStyle ';')?
		('cell' cellStyle=TableCellStyle ';')?
		(stripes?='stripes' (stripeColor=Color)? ';')?
		('header' header=STRING ';')?
		('accessory' accessory=MenuItemStandardButton ';')?
		(menuItems+=MenuItem*)
	'}'
	
;

Gallery:
	'gallery' name=VARID (frame=Frame)?  '{'
		('@' description=STRING ';')? 
		(isSelectable?='selectable' (multiple?='multiple')? key=STRING ';')?
		(rowStructure=[Spacer] arrayType=ArrayType ';')?
		('cellHeight' cellHeight=POSINTNUMBER ';')?
		('cellWidth' cellWidth=POSINTNUMBER ';')?
		('background' backgroundColor=Color ';')?
		('header' header=STRING ';')?
		('pages' pages=POSINTNUMBER ';')?
		(action=Action)?
	'}'
;

MoviePlayer:
	'player' name = ID (frame=Frame)?  '{'
		('@' description=STRING ';')? 
	    (isAutomatic?='play' ('(' inSeconds=POSINTNUMBER ')') ';')? 
		('start' url=STRING ';')?
		('load' file=STRING ';')?	
		('store' ('S3')? ('bucket')? bucketRef=[Text] ('filename') nameRef=[Text] ';')? 
		('record' recordedFile=STRING ('quality' quality=AudioQuality)? ('bitrate' bitrate=POSINTNUMBER)? ('channels' channels=POSINTNUMBER)? ('samplerate' samplerate=POSINTNUMBER)? )?
	'}'
;

enum BarcodeTypes: 
	upca | upce | ean13 | ean8 | qr
;

BarcodeScanner:
	'scanner' name = ID (bc?='barcode')? (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
		(barcodeType += BarcodeTypes ((',') barcodeType += BarcodeTypes)*)? (allTypes?='all' ';')?
		('image' image=STRING ';' ('selectedImage' selectedImage=STRING ';')?)?
		('title' title=STRING ';')?
		('color' color=Color ';')?
		('backgroundColor' backgroundColor = Color ';')?  
	'}')? (';')?
;
	
enum ButtonStyle:
	Bordered | Gradient | Custom | Navbar  //Checkbox | Switch | Plain  
;	

enum GradientStyle:
	Alert | RedDelete | White | Black | WhiteActionSheet | BlackActionSheet | SimpleOrange | GreenConfirm ;

enum KeyboardSide:
	Right | Left ;

enum KeyboardSideLC:
	right | left ;

enum StandardImage:
	None | DetailDisclosure | InfoLight | InfoDark | ContactAdd 
;

Button:
	'button' name=VARID (title=STRING (',' titleSelected=STRING)?)? (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
	
	    (('with')? 'style' textStyle=[Style])?
		(style = ButtonStyle (gradientStyle=GradientStyle)?  (isLeft?='left')? ';')? 
		('image' image=STRING ';')? 
		('selectedImage' selectedImage=STRING ';')? 
		('font' font=STRING ';')? 
		('size' size= POSINTNUMBER ';')? 
		('color' textColor=Color (textColorSelected=Color)? ';')?
		//('backgroundColor' backgroundColor=Color ';')?
		(action=Action)? 
		//(isFlex?='flex' ';')? 
		//(isTrash?='trash' ';')? 
		(roundedCorners=RoundedCorners ';')? 	
		//(isOnKeyboard?=KeyboardSide ';')? 
		(isStandard=StandardImage ';')?
		//(isOnNavBar?='navbar' ';')?
		(isUnbound?='without' (bindings?='bindings')? ';')?	
		('guard' performer=[Performer] 'binding' guardValue=[Text] ';')?
	'}')? (';')?
;	

enum ImageStyle:
	Thumbnail | BigThumbnail ;

S3:
	'bucket' bucketRef=[Text] 'filename' nameRef=[Text] ';'
;

FileStorage:
	S3 
;

Image:
	'image' name=VARID (title=STRING)? 	frame=FrameImage  ("{"  //'('(x=INTNUMBER)?','(y=INTNUMBER)?( hasWidth?=','(width=POSINTNUMBER)?','(height=POSINTNUMBER)?)?')'
	    ('@' description=STRING ';')? 
	    
	    (resource=STRING ';')?
		//(imageStyle=ImageStyle? ';')?
		(action=Action)?
		//(zoom?='canBeZoomed' ';')?
		(roundedCorners=RoundedCorners ';')?	
		('border' color=Color ('(' borderSize=POSINTNUMBER ')')? ';')?
		(isScreenWide?='screenWide' ';')?	
		('binding' someText = VARID '[' (words+=KeyValuePair (',' words+=KeyValuePair)*) ']' ';') ?	
		(isUnbound?='without' (bindings?='bindings')? ';')?	
		('store' ('S3')? ('bucket')? bucketRef=[Text] ('filename') nameRef=[Text] ';')? 
		('guard' performer=[Performer] 'binding' guardValue=[Text] ';')?
		'}')? (';')?
;

AxisDefinition:
	('title' xtitle=STRING  ';')? 
	'scale' min=FLOAT ',' interval=POSINTNUMBER ('(' minorTicks=POSINTNUMBER')')? ',' max=FLOAT (',' intersect=FLOAT)? ';'
;

enum PlotType:
	scatter | bar
;

enum PlotSymbol:
	ellipse | rectangle | diamond | triangle | star | pentagon | hexagon | cross | plus | dash | snow 
;

PlotDefinition:
	'plot' plotType=PlotType name=VARID ('binding' binding=VARID)? '{' (plotSpace?='right' ';')? ('width' lineWidth=POSINTNUMBER ';')? (color=Color (hasGradient?='gradient')?';')?  (symbol=PlotSymbol '(' symbolSize=POSINTNUMBER ')' ';')? '}' ';'  
;

enum GraphThemes:
	DarkGradient | PlainWhite | PlainBlack | Slate | Stocks
;

Graph:
	'graph' name=ID (frame=Frame)?  ("{"
		('@' description=STRING ';')? 
	
	    ('theme' theme=GraphThemes ';')?
		(plots+=PlotDefinition)+ 
	    ('padding' padding=Frame ';')? 
		('title' title=STRING (color=Colors)? (size=POSINTNUMBER)? ';')?
		('border' boderColor=Colors (borderSize=POSINTNUMBER)? ';')?
		('axes' '{'
			('x' '{' defx=AxisDefinition 'binding' boundTo=VARID ';' (customLabels?='custom' ';')? '}')?
			('y' '{' defy=AxisDefinition '}')?
			('yright' '{' defyLeft=AxisDefinition '}')?
		'}' ';')?
		(action=Action)?
		(zoom?='canBeZoomed' ';')?
		(roundedCorners=RoundedCorners ';')?		
		'}')? (';')?
;	
	
DatePicker:
	'datepicker' name=VARID (title=STRING)? (frame=Frame)?  (('as') asActionSheet?='popup')? ("{"
		('@' description=STRING ';')? 
	    (hasTime?='time' (only?='only')? ';')? 
	    (isTimer?='timer')?
		(action=Action)? 
		('border' border=TextBorder ';')? 
	"}")? (";")?
;	

	
Picker:
	'picker' name=VARID title=STRING (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
		('components' components=POSINTNUMBER ';')?
		('rows' row=POSINTNUMBER ';')?
		('data' '[' (rows+=StringArray ((',') rows+=StringArray)*) ']' ';') ?
		(buttonImage=STRING ';')?
		(action=Action)?	
	'}')? (';')?	
;	

ActionSheet:
	'actionSheet' message=STRING ('{'
		('@' description=STRING ';')? 
		('yesButton' yesButton=STRING ';')?
		('noButton' noButton=STRING ';')?
		('others' buttons+=STRING* ';')?
	'}')? (';')?	
;	

Alert:
	'alert' name=ID message=STRING ('{'
		('@' description=STRING ';')? 
		('title' title=STRING ';')?
		('timer' timer=STRING ';')?
		('button' button=STRING ';')?
		('others' buttons+=STRING* ';')?
	'}')? (';')?	
;	
	
ProgressBar:
	'progressBar' name=VARID (frame=Frame)?  ('{'
		('@' description=STRING ';')? 
		('timer' timer=STRING ';')?
	'}')? (';')?	
;	
	
enum Alignment:
	Right | Center | Left | Bottom | Top | Middle ;

Label:
	'label' name=VARID text=STRING (frame=Frame)?  ('{' 
		('@' description=STRING ';')? 
	    (('with')? 'style' style=[Style] ';')?
		('font' font=STRING ';')? 
		('size' size=POSINTNUMBER ';')?
		('color' color=Color ';')?
		//('background' backgroundColor=Color ';')?
		//(isBold?='bold' ';')?
		//(isItalic?='italic' ';')?
		//(hasShawdow?='shadow' '('offsetX=FLOAT','offsetY=FLOAT')' ';')?
		(alignment=Alignment ';')?		
		('lines' lines=POSINTNUMBER ';')?
		(isUnbound?='without' (bindings?='bindings')? ';')?	
		(boundTo?='bound' 'to' (vars+=[Text] ((',') vars+=[Text])*) ('separator' sep=STRING)? ';' )?
		'}') (';')?
;		
	
ViewFrame:
	(isScrollFrame?='scroll')? 'frame' (frame=Frame) ';';
	
enum YesNo :
	yes='yes' | no='no';
	
PreDefColor:
	preDefColor=Colors
;

RGBA:
	rgba=RGB
;

Color:
	PreDefColor | RGBA ;
	
RGB:
	'rgb' (name=ID)? '(' r=POSINTNUMBER ',' g=POSINTNUMBER ',' b=POSINTNUMBER (',' alpha=POSINTNUMBER)? ')' 
;

LayoutRef:
	'layout' layout=[Spacer] ('bindings' connection=[Connection] ('<' (params+=ID ((',') params+=ID)*) '>')? ('use')? ('mapping' mapping=[Mapping])? ('initializeWith' initOperation = VARID )?)? ';'
;



Status:
	ID ;

enum AnnotationEvents:
	RecordSelected | RightButton
;

enum ButtonEvents:
	Touched ;

ApplicationEvents:
	event=AnnotationEvents (button=[Button]'.' buttonEvent=ButtonEvents)?
;

Selected:
	'(' selectedView=[View] '[' (selectedIndex?='selectedIndex')? (index=POSINTNUMBER)? ']' ')'
;

StateAssignment:
	'<'var=Status '=' value=STRING '>' ';'
;

ViewEvent:
	'on' event=ApplicationEvents 'push' view=[View](selected=Selected)?
	 ('{' (isAnimated?='animated' ';')? 
	 	  (assignment=StateAssignment)?
	 '}' ';')?
;

enum PointOfInterest:
	UserLocation ;

Navigation:
	'navigate' title=STRING 'push' view=[View](selected=Selected)?
		 ('{' (isAnimated?='animated' ';')? 
	 	  (assignment=StateAssignment)?
	 '}' ';')?
;

View :
	'view' name=ID (title=STRING)?  ((usesUserDefault?='uses')? 'user' isUserDefault?='defaults')? //('<'status=Status'>')? ('extends' extends=[View])? 
	'{'
		('@' description=STRING)?
	    (isScrollView?='scroll' (height=POSINTNUMBER)?  (bounces?='bounces')? ';')?  //('zoom' '(' min=FLOAT ',' max =  FLOAT ')')?
	    
	    //(annotations=Annotations ';')?
	    //('display' poi=PointOfInterest ';')?
	    ('backgroundImage' image=STRING ';')?
	    ('color' backgroundColor=Color ';')?
	    //(properties+=ViewProperty)*
		//(simpleProperties+=SimpleProperty)*
        //('subviews' '{' (subviews+=[View])+ '}' ';')?
        //('pages' '{' (pages+=[View])+ '}' ';')?
        (readOnly?='readOnly')?
        ('controls' (layout=SpacerType)? '{' 
            (layouts+=LayoutRef)+ 
        	//(viewEvents+=ViewEvent)*
        	('returns' params+=ActionParameter ((',') params+=ActionParameter)* ';')?
        '}' (';')?)?
        //(navigation=Navigation)?
        //(animation=ViewAnimation)?
        ('icon' icon=STRING ';')?
        ('hide' (hidetabs?='tabs')?  (hidebar?='bar')? ';')?
        //('show' activityIndicator?='activity' ('(' activityColor=Color ')')? ';')?
        ('back' backButton=STRING ';')?
        //(autorotate?='autorotate' ';')?
        (('requires')? hasInstance?='instance' ('with' hasLogin?='login' ('at' isAlways?='start')? 'bindings' connection=[Connection] (('use')? 'mapping' mapping=[Mapping])?)? ';')?
        ('title' (imageTitle=STRING ';') ('font' titleFont=STRING ';')?  ('size' titleSize=POSINTNUMBER ';')? ('color' titleColor=Color ';')? ('shadow' titleShadowColor=Color ';')?  )?
	'}';
	
//******************************************************************************
// Connections
//
//******************************************************************************

enum VERB:
	GET | POST | PUT | DELETE | HEAD | COMMAND | CLOSE
;

enum CRUD:
	QUERY | CREATE | UPDATE | DELETE  
;
enum Protocol: HTTP | SOAP | RSS | ATOM | TCP ;

enum ProtocolVersion: v12='soap12' | v11='soap11' | v2='rss2' | v1='rss1' ;

Header:
	'header' name=STRING ':' value=STRING ';' ;

Operation:
	'operation' (isInit?='init')? name=ID verb=VERB url=STRING (params+=QualifiedName ((',')? params+=QualifiedName)*)? ('{' 
		('@' description=STRING ';')?
	
		('protocol' protocol=Protocol (version=ProtocolVersion)? ('SOAPAction' soapAction=STRING)? ';')? 
		('host' host=STRING ('('port=POSINTNUMBER')')? ';')?
		(isJson?='json' ';')? 
		(isCsv?='csv' ';')?  
		(isAsynchronous?='asynch' ';')? 
		('request' requestElement=STRING ';')? ('namespace' namespace=STRING ';')? 
		('timeout' timeout=POSINTNUMBER ';')? 
		('resultSet' (isImage?='image')? resultSet=STRING ('(' rowElement=STRING ')')?';')? 
		('triage' (sortValues+=VARID)*  (isAscending?='ascending')? ';')? 
		(isWrapped?='wrapped' ';')?
		('resultPick' (resultParams+=QualifiedName ((',') resultParams+=QualifiedName)*)? ';')?
		(isCashed?='cache' ( '(' duration= POSINTNUMBER (',' storeLocally?='local')? ')' )? ';' )?  
		(freezable?='deferred' ';')?
		(isAsynch?='asynchronously' ';' )?
		('join' joinedOp=[Operation] ('on' id1=VARID '=' id2=VARID)?  ('where' key=QualifiedName '=' value=STRING)? ';')? 
		('redirect' urlRedirect=STRING ';')?
		(headers+=Header)* 
		('authentication' isBasic?='basic' username=STRING password=STRING ';')?
		'}')?
;

Mapping:
	'mapping' name = ID '{'
		(pairs+=KeyValuePair (',' pairs+=KeyValuePair)*)
	'}' 
;

enum Format: xml | json | csv ;

Connection:
	'connection' name=ID (isLocal?='local')? '{'
	('@' description=STRING ';')?
	
		(operations+=Operation (';')?)*	
		(dataSource=DataSource (';')?)? 
		//(entities+=EntityOperation)*	
		//(format=Format otherDataSource=STRING ';')?
		//(mapping=[Mapping] ';')?
	'}'
;

Wifi:
 isWifiOnly?='wifi'
;

Data:
	isDataOnly?='data'
; 
 
enum Network:
	none | both | wifi | data 
; 

Main:
	'main' name=ID (displayName=STRING)? '{'
	('@' description=STRING ';')?
	
	('splashscreen' splashScreen=STRING ('('duration=STRING ')')? ';')?
	(hasNavigationBar='navigationBar' ('(' navigationBarColor=Color ')')? ';')?
	//(autorotate?='autorotate' ';')?
	('network' network=Network  ';')?
	
	('start' startView=[View] ';')?
	
	'menu' ('(' tabBarColor=Color (',' tabBarBck=STRING ',' tabBarBckSelected=STRING)? ')')? '{' 
		(views+=[View] ((',') views+=[View])*) '}'
	
	('split' '{' leftView=[View] (',' rightView=[View])? '}') ?
	
	('data' '{' (entities+=[DataType] ((',')entities+=[DataType])*) '}' )?

	//('processes' '{' (processes+=[Process] ((',') processes+=[Process])*) '}' )?
	
	('version' version=STRING '{' 'build' build=STRING ';' '}')?
		
	//('roll' orl=STRING '{' (rolls+=KeyValue (','rolls+=KeyValue )* '}')? ';')?
	
	(debug?='debug' ';' )?
	
	'}'
	
;


/* Release notes
V1.0
________________________________________________________________________
Action 			: there was a spelling error for the "update" element ;
DatePicker		: keyword is all lowercase (datePicker -> datepicker)

TBI -> to be implemented
V1.1
________________________________________________________________________
Operation		: resultSet -> added image type; //non breaking //TBI
				  added isCashed?='cache' ( '(' duration= POSINTNUMBER ')' )? 
				  (isAsynch?='asynchronously' ';' )?
Action			: added new standard action addEventToCalendar  //TBI
Style			: added list of styles to the model //TBI
                  added style ref to label, text, button
Map				: added  ('(' locName=STRING ',' locAddress=STRING ')')? ot location
View        	: added (hidetabs?='hide' 'tabs' ';')?
                : added "back" button
Spacer	        : 	('tablet' tabletLayout=[Spacer])?
	('down' downLayout=[Spacer])?
	('left' leftLayout=[Spacer])?
	('right' rightLayout=[Spacer])?
main/splashscreen : duration

V1.2
__________________________________________________________________________
LayoutRef       : ('initializeWith' initOperation = [Operation] )?)?
Map		        : ('locationReference' locLatRef=[Text] ',' locLongRef = [Text] ('(' locNameRef = [Text] ',' locAddressRef = [Text] ')')? ';')?
Table 			: added key (isSelectable?='selectable' (multiple?='multiple')? key=STRING ';')?
Gallery			: new control
Operation		: added join to make an additional call which will join the resultset to the initial one
RadioButton		: as star attribute, added individual values
Action			: show direction, complete activity, showAlert
Main			: added instance concept, with optional login
Button		    : added navbar attribute
Main			: tabBarColor=Color 
View		    : (('requires')? hasInstance?='instance' ('with' hasLogin?='login' ('at' isAlways?='start')? 'bindings' connection=[Connection] (('use')? 'mapping' mapping=[Mapping])?)? ';')?
                  ('title' (imageTitle=STRING ';')? ('font' titleFont=STRING ';')?  ('size' titleSize=STRING ';')? ('color' titleColor=STRING ';')? ('shadow' titleShadowColor=STRING ';')?  )?
Style			: has been implemented
Switch/Slide	: color
Tabbar 			: color
Breaking Navbar color

V1.3
__________________________________________________________________________
Action			: shareUrl, fields, shareImage
Operation		: support for sockets, timeout, headers
Action			: 		(dismiss?='dismiss')?  (imageToShare=[Image])? in shareInfo
 						('present' presentView=[View])?
Main			: ('menu' (',' tabBarBck=STRING ',' tabBarBckSelected=STRING)? 
Action   		:	('shareInformation' informationToShare=[Text] (shareUrl=STRING)?)? ('on' (twitter?='twitter')? ((',')?facebook?='facebook')? ((',')? shareEmail?='email')? ((',') delicious?='delicious')? )?
Button			: Navbar left ;    
Browser		    : background color 
Operation 		: (isWrapped?='wrapped' ';')?  timeout

V1.4
__________________________________________________________________________
Text			: as date, format           
AdBanner
Operation		: sort results, Basic Auth    
Action		    : (updateView?='updateView')? , present for seconds, invokeMethod
                  ('playSound' soundPlayer=[MoviePlayer])?
deferred	    : operation                  
*/
