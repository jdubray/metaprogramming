/*
 * generated by Xtext
 */
package com.canappi.metamodels.mdsl;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class MDslRuntimeModule extends com.canappi.metamodels.mdsl.AbstractMDslRuntimeModule {

}
