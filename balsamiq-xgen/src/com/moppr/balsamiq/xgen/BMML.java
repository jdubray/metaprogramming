package com.moppr.balsamiq.xgen;

public interface BMML {

		static String _control = "control";
		static String _groups = "groupChildrenDescriptors";
		static String _controlId = "controlTypeID";
		static String _x = "x";
		static String _y = "y";
		static String _width = "measuredW" ;
		static String _height = "measuredH" ;
		static String _properties = "controlProperties";
		static String _text = "text";
		static String _src = "src";
		static String _selectedIndex = "selectedIndex";
		static String _iconName = "icon";
		static String _color = "color";
		
		static String _view = "com.balsamiq.mockups::iPhone" ;
		static String _button = "com.balsamiq.mockups::Button" ;
		static String _textField = "com.balsamiq.mockups::TextInput" ;
		static String _label = "com.balsamiq.mockups::Label" ;
		static String _image = "com.balsamiq.mockups::Image";
		static String _picker = "com.balsamiq.mockups::ComboBox";
		static String _switch = "com.balsamiq.mockups::Switch";
		static String _menu = "com.balsamiq.mockups::MenuBar";
		static String _iPhone = "com.balsamiq.mockups::iPhone";
		static String _iconAndLabel = "com.balsamiq.mockups::IconLabel" ;
		static String _icon = "com.balsamiq.mockups::Icon" ;
		static String _browser = "com.balsamiq.mockups::BrowserWindow";
		static String _link ="com.balsamiq.mockups::Link";
		static String _paragraph ="com.balsamiq.mockups::Paragraph";
		static String _canvas = "com.balsamiq.mockups::Canvas";
		static String _datePicker = "com.balsamiq.mockups::iPhonePicker";
		static String _pointyButton = "com.balsamiq.mockups::PointyButton" ;
		static String _checkBox = "com.balsamiq.mockups::CheckBox";
		static String _radioButton = "com.balsamiq.mockups::RadioButton";
		static String _slider = "com.balsamiq.mockups::HorizontalScrollBar";
		static String _slider2 = "com.balsamiq.mockups::HSlider";
		static String _slider3 = "com.balsamiq.mockups::VSlider";
		static String _table = "com.balsamiq.mockups::DataGrid";
		static String _searchBox = "com.balsamiq.mockups::SearchBox";
		static String _map = "com.balsamiq.mockups::Map";
		static String _radioButtonGroup = "com.balsamiq.mockups::RadioButtonGroup";
		static String _web = "com.balsamiq.mockups::BrowserWindow" ;
		static String _tableMenu = "com.balsamiq.mockups::Menu";
}
