package com.canappi.mockup.ib.xgen;


import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Xgen implements IB {
	static ArrayList<String> layouts ; 
	static ArrayList<String> views ; 
	static ArrayList<String> connections ; 
	static String main ;

	static ArrayList<IBView> appViews ; 

	
	public final static String resourcify(String source) {
		String replacements[] = {"!", "*", "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]", "  "};
		for (int i = 0 ; i < replacements.length; i++) 
		{
			source = source.replace(replacements[i], "") ;
		}
		source = source.replace(" ", "_") ;
		if (source.lastIndexOf("_") ==  source.length()-1) return source.substring(0, source.length()-1) ;
		return source ;
	}

	public static Integer findNextObjectWithAttribute(NodeList l, String forAttr, String key, Integer from) {
		Integer s = from ;
		while (s<l.getLength()) {
			Node n = l.item(s) ;
			NamedNodeMap attrs = n.getAttributes() ; 
			if (attrs != null) {
				Node a = attrs.getNamedItem(forAttr) ;
				if (a != null) {
					if (a.getNodeValue().equals(key)) {
						return s ;
					}
				}
			}
			s++ ; 
		}
		return null ; 
	}

	public static Integer findNextKey(NodeList l, String key, Integer from) {
		Integer s = from ;
		while (s<l.getLength()) {
			Node n = l.item(s) ;
			NamedNodeMap attrs = n.getAttributes() ; 
			Node a = attrs.getNamedItem(_key) ;
			if (a.getNodeValue().equals(key)) {
				return s ;
			}
			s++ ; 
		}
		return null ; 
	}

	public static Integer findNextObject(NodeList l, Integer from) {
		Integer s = from ;
		while (s<l.getLength()) {
			Node n = l.item(s) ;
			//System.out.println(n.getClass().getName()+">>"+n.toString());
			if (n.getNodeName().equals(_object)) {
				return s ;
			}
			s++ ; 
		}
		return null ; 
	}

	
	public static ArrayList<String> parseFrame(String frame) {
		ArrayList<String> u = new ArrayList<String>() ;
		if (frame != null) {
			String x = frame.substring(2,frame.indexOf(",")) ;
			u.add(x) ;
			Integer middle = frame.indexOf("}, {") ;
			String y = frame.substring(frame.indexOf(",")+1,middle) ;
			u.add(y) ;
			String width = frame.substring(middle+4,frame.indexOf(',', middle+4)) ;
			u.add(width) ;
			String height = frame.substring(frame.indexOf(',', middle+4)+1,frame.length()-2) ;
			u.add(height) ;
		} else { u.add("10"); u.add("10"); u.add("50"); u.add("30") ; }
		return u ;
	}
	
	public static Integer findNextElement(NodeList l, String e, Integer from) {
		Integer s = from ;
		while (s<l.getLength()) {
			Node n = l.item(s) ;
			//System.out.println(n.getClass().getName()+">>"+n.toString());
			if (n.getNodeName().equals(e)) {
				return s ;
			}
			s++ ; 
		}
		return null ; 
	}
	
	public static Integer findNextElementWithValue(NodeList l, String e, String v, Integer from) {
		Integer s = from ;
		while (s<l.getLength()) {
			Node n = l.item(s) ;
			//System.out.println(n.getClass().getName()+">>"+n.toString());
			if (n.getNodeName().equals(e)) {
				Element el = (Element)n;
				
				String c = el.getTextContent() ;
				if (c.equals(v)) {
					return s ;
				}
			}
			s++ ; 
		}
		return null ; 
	}

	public static String findValueForKey(NodeList nl , String key, Integer index) {

		  Element keyEl = (Element)nl.item(index) ;
		  String value = keyEl.getTextContent() ;
		  System.out.println(key+":"+value);
		  return value ;
	}
	
	public static Integer findIndexForKey(NodeList nl , String key, Integer start) {

		  Integer brandNodeIndex = findNextElementWithValue(nl, _key, key , start) ;
		  if (brandNodeIndex != null) { 
			  brandNodeIndex = findNextElement(nl, _string , brandNodeIndex+1) ;
		  } else { 
			  System.out.println("Cannot find :"+key) ;
		  }
		  return brandNodeIndex ;
	}
	
	public static String findValueForKey(NodeList nl, String key) {
		
		Integer index = findIndexForKey(nl,key,0) ;
		return findValueForKey(nl,key,index) ;
	}

	public static String findValueForAttributeKey(NodeList nl, String key) {
		
		Integer index = findNextObjectWithAttribute(nl,_key,key,0) ;
		if (index == null) return null;
		return findValueForKey(nl,key,index) ;
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			  File file = new File(args[0]);
			  FileWriter outFile = new FileWriter(args[1]);
			  PrintWriter out = new PrintWriter(outFile);
			  
			  appViews = new ArrayList<IBView>();
			  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			  DocumentBuilder db = dbf.newDocumentBuilder();
			  Document doc = db.parse(file);
			  doc.getDocumentElement().normalize();
			  System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			  NodeList nodeLst = doc.getDocumentElement().getChildNodes();
			  Node data = null;
			  for (int n = 0 ; n < nodeLst.getLength() ; n++) {
				  Node u = nodeLst.item(n) ;
				  if (u.getNodeName().equals(_data)) data = u ;
			  }
			  if (data == null) throw new Exception() ;
			  NodeList objects = data.getChildNodes() ;
			  System.out.println(data.getNodeName()+" "+String.valueOf(objects.getLength()));
			  for (int i = 0 ; i < objects.getLength() ; i++) {
				  Node object = objects.item(i) ;
				  System.out.println("Found Element :" + object.getNodeName()+object.getClass().toString());
				  if (object.getNodeName().equals(_object)) {
				  NodeList cObjects = object.getChildNodes() ;
				  
				  Integer viewIndex = 0 ; 
				  while (viewIndex != null) {
					  viewIndex = findNextObjectWithAttribute(cObjects,_class,_IBUIView,viewIndex) ;
					  if (viewIndex != null) {
						  //get the controls for each view
						  IBView v = new IBView() ;
						  appViews.add(v) ;
						  NodeList viewElements = cObjects.item(viewIndex).getChildNodes() ;
						  Integer controlIndex = findNextObjectWithAttribute(viewElements,_key,_NSSubviews,0) ;
						  if (controlIndex != null) { 
							  NodeList controlList = viewElements.item(controlIndex).getChildNodes() ;
							  controlIndex = 0;
							  while (controlIndex != null) {
								  controlIndex = findNextObject(controlList,controlIndex) ;
								  if (controlIndex != null) {
									  Node n = controlList.item(controlIndex) ;
									  NodeList params = n.getChildNodes() ;
									  NamedNodeMap attrs = n.getAttributes() ; 
									  String controlType = attrs.getNamedItem(_class).getNodeValue() ;
									  if (controlType.equals(_IBUILabel)) {
										  Node l = params.item(findNextObjectWithAttribute(params, _key , _IBUIText, 0)) ;
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBUILabel,parseFrame(f.getTextContent())) ;
										  c.text = l.getTextContent() ;
										  v.addControl(c);
									  }
									  if (controlType.equals(_IBUIButton)) {
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBUIButton,parseFrame(f.getTextContent())) ;
										  Integer title = findNextObjectWithAttribute(params, _key , _IBUINormalTitle, 0) ;
										  if (title != null) {
											  c.text = params.item(title).getTextContent();
										  }
										  Integer selectedTitle = findNextObjectWithAttribute(params, _key , _IBUISelectedTitle, 0) ;
										  if (selectedTitle != null) {
											  c.textSelected = params.item(selectedTitle).getTextContent();
										  }
										  
										  Integer image = findNextObjectWithAttribute(params, _key , _IBUINormalImage, 0) ;
										  if (image != null) {
											  NodeList cs = params.item(image).getChildNodes() ;
											  String respos = findValueForAttributeKey(cs, _NSResourceName) ;
											  if (respos != null) {									  
												  c.image = respos;
											  }
										  }
										  Integer selectedImage = findNextObjectWithAttribute(params, _key , _IBUISelectedImage, 0) ;
										  if (selectedImage != null) {
											  NodeList cs = params.item(selectedImage).getChildNodes() ;
											  String respos = findValueForAttributeKey(cs, _NSResourceName) ;
											  if (respos != null) {									  
												  c.selectedImage = respos;
											  }
										  }

										  v.addControl(c);
														  
									  }
									  if (controlType.equals(_IBUISegmentedControl)) {
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBUISegmentedControl,parseFrame(f.getTextContent())) ;
										  v.addControl(c);
														  
									  }
									  if (controlType.equals(_IBUISlider)) {
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBUISlider,parseFrame(f.getTextContent())) ;
										  Integer value = findNextObjectWithAttribute(params, _key , _IBUIPlaceholder, 0) ;
										  if (value != null) {
											  c.text = params.item(value).getTextContent();
										  }
										  v.addControl(c);
														  
									  }
									  if (controlType.equals(_IBUITextField)) {
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBUITextField,parseFrame(f.getTextContent())) ;
										  Integer placeHolder = findNextObjectWithAttribute(params, _key , _IBUIValue, 0) ;
										  if (placeHolder != null) {
											  c.text = params.item(placeHolder).getTextContent();
										  }
										  v.addControl(c);
														  
									  }
									  if (controlType.equals(_IBADBannerView)) {
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBADBannerView,parseFrame(f.getTextContent())) ;
										  v.addControl(c);
														  
									  }
									  if (controlType.equals(_IBUIImageView)) {
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBUIImageView,parseFrame(f.getTextContent())) ;
										  v.addControl(c);
														  
										  Integer image = findNextObjectWithAttribute(params, _key , _IBUISelectedImage, 0) ;
										  if (image != null) {
											  NodeList cs = params.item(image).getChildNodes() ;
											  Integer respos = findNextObjectWithAttribute(cs, _key , _NSResourceName, 0) ;
											  if (respos != null) {
												  Node res = params.item(respos) ;
												  c.image = res.getTextContent();
											  }
										  }
										  Integer selectedImage = findNextObjectWithAttribute(params, _key , _IBUINormalImage, 0) ;
										  if (selectedImage != null) {
											  NodeList cs = params.item(selectedImage).getChildNodes() ;
											  Integer respos = findNextObjectWithAttribute(cs, _key , _NSResourceName, 0) ;
											  if (respos != null) {
												  Node res = params.item(respos) ;
												  c.selectedImage = res.getTextContent();
											  }
										  }
									  }
									  if (controlType.equals(_IBUIProgressView)) {
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBUIProgressView,parseFrame(f.getTextContent())) ;
										  v.addControl(c);
														  
									  }
									  if (controlType.equals(_IBUIActivityIndicatorView)) {
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBUIActivityIndicatorView,parseFrame(f.getTextContent())) ;
										  v.addControl(c);
														  
									  }
									  if (controlType.equals(_IBUISwitch)) {
										  Node f = params.item(findNextObjectWithAttribute(params, _key , _NSFrame, 0)) ;
										  IBObject c = new IBObject(_IBUISwitch,parseFrame(f.getTextContent())) ;
										  Integer on = findNextObjectWithAttribute(params, _key , _IBUIOn, 0) ;
										  if (on != null) {
											  c.text = params.item(on).getTextContent();
										  }
										  v.addControl(c);
																					  
									  }
									  controlIndex++;
								  }
							  }
							  
						  }
					  }
					  if (viewIndex != null) viewIndex++;
				  } 
				  }
			  }
				  			  
			  views = new ArrayList<String>();
			  layouts = new ArrayList<String>();
			  connections = new ArrayList<String>();
			  
			  String backgroundColor = "FFFFFF" ;
			  
			  
			  String mapping = "mapping fbMapping { {'id' : 'id'} , {'message.text' : 'message'} , { 'picture' : 'picture'} , { 'message.url' : 'link'}, { 'description' : 'description'}, { 'created' : 'created_time' } } \n\n";
			  mapping += "mapping blogMapping { {'postTitle.text' : 'title'} , { 'description' : 'description'} , { 'pubDate' : 'pubDate'}, { 'postTitle.url' : 'link'}, { 'id' : 'guid' } } \n\n";
			  mapping += "mapping twitterMapping { {'id' : 'id'} , { 'description' : 'text'} , { 'pubDate' : 'created_at'}, { 'handle' : 'user.name'}} \n\n";
			  mapping += "mapping youTubeMapping { {'postTitle.text' : 'content'} , { 'pubDate' : 'published'}, { 'postTitle.url' : 'id'}, { 'id' : 'id' } } \n\n";	
			  mapping = "" ;
			  
			  main ="main yourAppName {\n";
			  main+="\n";
			  main+="		splashscreen 'yourSplashScreen.png' ;\n";
			  main+="		navigationBar ;\n";
			  main+="		start view0 ;\n";
			  main+="		menu {  " ;
			  String cm = "" ;
			  Iterator<IBView> iter = appViews.iterator() ;
			  Integer viewCounter = 0 ;
			  while (iter.hasNext()) {
				  IBView m = iter.next() ;
				  String layout = "" ;
				  String view = "" ;
				  String connection = "" ;
				  connection = "connection myView"+String.valueOf(viewCounter)+"Model { \n";
				  connection += "	operation init getmyViewModel GET 'your url goes here' {\n";
				  connection += "		resultSet 'data' ;\n";
				  connection += "	} ;\n";
				  connection += "}\n" ;
				  
				  layout = "layout view"+String.valueOf(viewCounter)+"Layout {\n"; 
				  ArrayList<IBObject> controls = m.controls ;
				  for (int u = 0 ; u < controls.size(); u++) {
					  layout += controls.get(u).serialize(_mdsl) ;
				  }
				  layout += "}\n";
				  layout += "\n";
				  
				  view  ="view view"+String.valueOf(viewCounter)+"   'Your View Title'  {\n";
				  view +="	  \n";
				  view +="		color groupTableViewBackground ;\n";
				  view +="		controls {\n";
				  view +="			layout view"+String.valueOf(viewCounter)+"Layout ; " ;//    bindings myView"+String.valueOf(viewCounter)+"Model    mapping blogMapping;\n";
				  view +="		}\n";
				  view +="		icon 'your-view-glyph.png';\n";
				  view +="}\n";
				  
				  main += cm+"view"+String.valueOf(viewCounter) ; cm = ", " ;


				  connections.add(connection) ;
				  layouts.add(layout) ;
				  views.add(view);
				  viewCounter++;
			  }
			  main+="		} \n";
			  main+="		\n";
			  main+="}\n\n";
			  
			  out.println("// This mdsl code was generated from a XIB File (Interface Builder)\n\n");
			  out.println("//  \n");
			  out.println("// Please replace all 'Your' strings and resource names by your own\n\n\n\n");
			  out.println("package com.yourcompany.yourapp ;\n\n\n");
			  
			  /*
			  out.println("// Examples of Connection Definitions\n\n");
			  for (int l = 0 ; l < connections.size(); l++) {
				  String connection = connections.get(l);
				  out.println(connection) ;
			  }
			  
			  out.println("\n\n// Examples of Mapping Definitions\n\n");
			  out.println(mapping) ;
			  */
			  out.println("\n\n// Your Layout definitions \n\n");
			  for (int l = 0 ; l < layouts.size(); l++) {
				  String layout = layouts.get(l); 
				  out.println(layout) ;
			  }
			  out.println("\n\n// Your View definitions \n\n");
			  for (int v = 0 ; v < views.size(); v++) {
				  String view = views.get(v) ;
				  out.println(view) ;
			  }
			  out.println("\n\n// The Application Main Definition \n\n");
			  out.println(main) ;

			  
			  out.close();

			  } catch (Exception e) {
			    e.printStackTrace();
			  }	
		}

}
