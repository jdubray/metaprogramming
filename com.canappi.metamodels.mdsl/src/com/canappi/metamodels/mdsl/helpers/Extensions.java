package com.canappi.metamodels.mdsl.helpers;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.canappi.metamodels.mdsl.mDsl.*;
import com.canappi.metamodels.mdsl.mDsl.impl.*;
import com.ibm.icu.math.BigDecimal;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;


public class Extensions {

	public enum Platforms { iOS, SYMBIAN360x640 , balsamiq, android320x480, android480x854, android480x800, android1024x600   } 
	   private static int counter = 0;
	   private static final String _NO = "NO";
	   private static final String _YES = "YES";
	   private static String readOnly = _NO;
	   private static String roOwner = "";
	   
	   private static String x = "0";
	   private static String y = "0";
	   private static String width = "0";
	   private static String height = "0";
	   
	   private static HashMap<String,String> h ;
	   private static EList<View> queue ;
	   private static View current ; 
	   private static Mapping currentMapping ; 
	   private static HashMap<String,EList<View>> queues ;

	   private static Platforms platform = Platforms.iOS; 

	   private static String packageName = "com.canappi.metamodels.mdsl.mDsl.impl.";
	   
	   private static boolean debug = false;
	   
	   private static Color navBarColor ;
	   
	   private static void log( boolean force, String s){
		   
		   
		  if (debug | force) System.out.println(s);
		   
	   }
	   
		public final static void setMapping(Mapping m) {
			if (m != null) { log( false, "Setting mapping"+m.getName()) ; } 
			else { log( false, "mapping is null");}
			currentMapping = m;
			
		}
		
		public final static Mapping getMapping(){
			if(currentMapping != null ) { log( false, currentMapping.getName()) ;}
			else { log( false, "currentMapping is null") ;}
			return currentMapping;
		}
		
		public final static String getMappingName(){
			return currentMapping.getName();
		}

		public final static String processKeyword2(String var){	
			if (var.startsWith("_")) return var.substring(1) ;
			return var ;
		}

		public final static String escapeDoubleQuotes(String var){	
			String result = var.replace("\"", "\\\"") ;
			return result ;
		}

		public final static String escapeAmp(String var){	
			String result = var.replace("&", "&amp;") ;
			return result ;
		}

		public final static String processKeyword(String var){
			StringBuffer dot = new StringBuffer() ;
			for (int i = 0 ; i < var.length(); i++) {
				if (var.charAt(i) == '.') {
					dot.append("_") ;
				} else {
					dot.append(var.charAt(i)) ;
				}
			}
			var = new String(dot) ;
			if (var.startsWith("_")) return var.substring(1) ;
			
			return var ;
		}

		public final static String getCharSizeFor(String pixelWidth) {
			int symbian = 9 ;
			int android1 = 9 ;
			int android2 = 9 ;
			int android3 = 9 ;
			if (platform == Platforms.SYMBIAN360x640) {
				int pixels = Integer.valueOf(pixelWidth) ;
				int chars = 1 + (pixels/symbian) ;
				return String.valueOf(chars) ;
			}
			if (platform == Platforms.android320x480) {
				int pixels = Integer.valueOf(pixelWidth) ;
				int chars = 1 + (pixels/android1) ;
				return String.valueOf(chars) ;
			}
			if (platform == Platforms.android480x800) {
				int pixels = Integer.valueOf(pixelWidth) ;
				int chars = 1 + (pixels/android2) ;
				return String.valueOf(chars) ;
			}
			if (platform == Platforms.android480x854) {
				int pixels = Integer.valueOf(pixelWidth) ;
				int chars = 1 + (pixels/android3) ;
				return String.valueOf(chars) ;
			}
			return pixelWidth ;
		}

		public final static String getCharHeightFor(String pixelHeight) {
			int symbian = 28 ;
			int android1 = 28 ;
			int android2 = 28 ;
			int android3 = 28 ;
			if (platform == Platforms.SYMBIAN360x640) {
				int pixels = Integer.valueOf(pixelHeight) ;
				int rows = (pixels/symbian) ;
				return String.valueOf(rows) ;
			}
			if (platform == Platforms.android320x480) {
				int pixels = Integer.valueOf(pixelHeight) ;
				int rows = (pixels/android1) ;
				return String.valueOf(rows) ;
			}
			if (platform == Platforms.android480x800) {
				int pixels = Integer.valueOf(pixelHeight) ;
				int rows = (pixels/android2) ;
				return String.valueOf(rows) ;
			}
			if (platform == Platforms.android480x854) {
				int pixels = Integer.valueOf(pixelHeight) ;
				int rows = (pixels/android3) ;
				return String.valueOf(rows) ;
			}
			return pixelHeight ;
		}

		

		public final static void setPlatform(String newPlatform) {
				if (newPlatform.equals("Symbian-360x640")) { 
					platform = Platforms.SYMBIAN360x640;
					log( false, "System is Symbian");
				}
				else
				if (newPlatform.equals("Android-320x480")) { 
					platform = Platforms.android320x480;
					log( false, "System is Android");
				}
				else
				if (newPlatform.equals("Android-480x800")) { 
					platform = Platforms.android480x800;
					log( false, "System is Android");
				}
				else
				if (newPlatform.equals("Android-480x854")) { 
					platform = Platforms.android480x854;
					log( false, "System is Android");
				}
				else platform = Platforms.iOS;
		}

		public final static View getCurrentView() {
			return current;		
		}

		public final static String getCurrentViewName() {
			if (current != null) return current.getName();	
			else return "ERROR: current view not set";
		}

		public final static View setCurrentView(View v) {
			current = v;
			return current;
		}

		public final static String stripExtension(String h) {
			int l = h.lastIndexOf(".") ;
			if (l>0) return h.substring(0,l) ; else return h ;
		}

		public final static String isAudio(String h) {
			String xt = fileExtension(h) ;
			
			if (xt.equals("mp3")) return _YES ;
			if (xt.equals("mp4")) return _YES ;
			if (xt.equals("ogg")) return _YES ;
			if (xt.equals("wav")) return _YES ;
			if (xt.equals("aac")) return _YES ;
			if (xt.equals("caf")) return _YES ;
			
			return _NO ;
		}

		public final static String fileExtension(String h) {
			int l = h.lastIndexOf(".") ;
			if (l>0) return h.substring(l+1) ; else return "" ;
		}

		public final static String breakHTMLString(String hs) {
			StringBuffer r = new StringBuffer() ;
			String h = hs.replaceAll("\n", "") ;
			if (h.length()>80) 
			{
				int s = 0 ;
				int k = 80 ;
				while( k < h.length() ) {
					int  u = h.lastIndexOf(" ", k-5) ;
					if (u<=0) {
						if (k-5 < h.length()) {
							u = k-5 ;
						} else {
							u = h.length();
						}
							
					}
					r.append(h.substring(s, u)).append("\\\n") ;
					s = u ;
					k = u+80 ;
				}
				r.append(h.substring(s, h.length())) ;
					
			} else r.append(h) ;
			
			return r.toString() ;
		}
	   
	   
		public final static String stringOf(String c, String ofLength) {
			if (c != null) {
				if (ofLength!=null) {
					StringBuffer s = new StringBuffer();
					for (int i= 0 ; i < ofLength.length() ; i++ )
					{
						s.append(c);
					}
					return s.toString();
				}
			}
			return "";
		}

		public final static String queue(String name, View v) {
			if (queues == null) queues = new HashMap<String,EList<View>> ();
			queue = queues.get(name) ;
			if (queue == null) { 
				queue = new BasicEList<View>(); 
				queues.put(name, queue);
			}
			if (v != null) queue.add(v) ;
			return "";
		}

		public final static View dequeue(String name) {
			if (queues != null) {
				queue = queues.get(name) ;
				if (queue != null) {
					if (!queue.isEmpty()) return queue.remove(queue.size()) ;
				}
			}
			return null;
		}

		public final static String dequeue(String name, View v) {
			if (queues != null) {
				queue = queues.get(name) ;
				if (queue != null) {
					if (!queue.isEmpty()) { 
						boolean b = queue.remove(v) ; 
						if (b) return _YES;
					}
				}
			}
			return _NO;
		}

		public final static View getLastInQueue(String name) {
			if (queues != null) {
				queue = queues.get(name) ;
				if (queue != null) {
					if (!queue.isEmpty()) return queue.get(queue.size()) ;
				}
			}
			return null;
		}

		public final static EList<View> getQueue(String name) {
			if (queues != null) {
				queue = queues.get(name) ;
				if (queue != null) {
					if (!queue.isEmpty()) return queue ;
				}
			}
			return null;
		}
	
		public final static void resetQueue(String name) {
			if (queues != null) {
				queues.remove(name) ;
			}			
		}

		public final static void resetQueues() {
			queues = null;		
		}
	
		
	   public final static String setVar(String varName, String value) {
			if (h == null) h = new HashMap<String,String>(); 
			if (varName != null) h.put(varName, value) ;
			return "";
		}

	   public final static String setVar(String varName, Integer value) {
			return setVar(varName, String.valueOf(value)) ;
		}

	   public final static String stringValueOf(Integer value) {
			return String.valueOf(value.intValue()) ;
		}

		public final static String getVar(String varName) {
			if (h == null) h = new HashMap<String,String>(); 
			if (varName != null) {
				String v = h.get(varName) ;
				if (v == null) return "" ; 
				else return v ; 
			}
			return "";
		}

		public final static String resetVars() {
			h = null; 
			return "";
		}

		public final static String contains(String s1, String s2) {
			String contains = _NO; 
			if (s1.indexOf(s2)>=0) contains = _YES ;
			return contains;
		}

		protected static double strech() {
			double strech = 1 ;
			switch (platform) {
				case SYMBIAN360x640 : strech = 360.0/320.0;
				break;	
				case android320x480 : strech = 1;
				break;	
				case android480x854 : strech = 480.0/320.0;
				break;	
				case android480x800 : strech = 480.0/320.0;
				break;	
			}
			//log( false, "strech:"+String.valueOf(strech));
			return strech;
		}

		protected static String strech(Integer o) {
			return String.valueOf((int)(o*strech()));
		}

		protected static double strechY() {
			double strech = 1 ;
			switch (platform) {
				case SYMBIAN360x640 : strech = 640.0/480.0;
				break;	
				case android320x480 : strech = 1;
				break;	
				case android480x854 : strech = 854/480.0;
				break;	
				case android480x800 : strech = 800.0/480.0;
				break;	
			}
			//log( false, "strech:"+String.valueOf(strech));
			return strech;
		}

		protected static String strechY(Integer o) {
			return String.valueOf((int)(o*strechY()));
		}

		
		protected static int offset() {
			int offset = 0 ;
			switch (platform) {
				case SYMBIAN360x640 : offset = 80; break;
				case android320x480 : offset = 0; break;
				case android480x854 : offset = 0; break;
				case android480x800 : offset = 0; break;
			}
			//log( false, "strech:"+String.valueOf(strech));
			return offset;
		}

		protected static String offset(Integer o) {
			return String.valueOf(o+offset());
		}

	   public final static String sgetX(String o, String d) {
		   if (o==null) o = "0" ;
		   if (d==null) d = "0" ;
		   	return sgetX(Integer.valueOf(o),d) ;
	   }
		
	   public final static String sgetX(Integer o, String d) {
			if (o == 0) return valueOf( x,offset()+Integer.valueOf(d)) ;
			if (o == 9999) x = "0";
			else x = String.valueOf(o) ;
			return valueOf( x,offset()+Integer.valueOf(d)) ; 
		}

	   public final static String sgetY(String o, String d) {
		   if (o==null) o = "0" ;
		   if (d==null) d = "0" ;
		   	return sgetY(Integer.valueOf(o),d) ;
	   }

	   
	   public final static String sgetY(Integer o, String delta) {
			int d = (int)(9*Integer.valueOf(delta))/10;
			log( false, "delta = "+delta);
			if (o == 0) return String.valueOf(d+(int)(Integer.valueOf(y))); ;
			if (o == 9999) y = "0";
			else y = String.valueOf(o) ; 
			return String.valueOf(d+(int)(Integer.valueOf(o)));
		}

	   public final static String sgetX(String o) {
		    if (o== null) o = "0"; 
		   	return sgetX(Integer.valueOf(o)) ;
	   }
		
	   public final static String sgetX(Integer o) {
		   int offset = 0;	
		   switch (platform) { 
		   	case SYMBIAN360x640 : offset = 80; break ;
		   	case android320x480 : offset = 0; break ;
		   	case android480x854 : offset = 0; break ;
		   	case android480x800 : offset = 0; break ;
		   	
	   		}
			if (o == 0) return x ;
			if (o == 9999) x = "0";
			else x = String.valueOf(o+offset);
			return x; 
		}

	   public final static String sgetX(String o, Integer s) {
		    if (o== null) o = "0"; 
		   	return sgetX(Integer.valueOf(o),s) ;
	   }
		
	   public final static String sgetX(Integer o, Integer shift) {
		   //Add a shift for positioning a label next to a checkbox for instance
			return String.valueOf(shift+Integer.valueOf(sgetX(o))); 
		}

	   
	   protected final static String valueOf(String s, Integer d){
		   return String.valueOf(d+Integer.valueOf(s));
	   }
	   
	   public final static String sgetXWithOffset(String o, Integer s) {
		    if (o== null) o = "0"; 
		   	return sgetXWithOffset(Integer.valueOf(o),s) ;
	   }
		
	   public final static String sgetXWithOffset(Integer o, Integer d) {
			if (o == 0) return valueOf(x,offset()+d) ;
			if (o == 9999) x = "0";
			else x = String.valueOf(o);
			return valueOf(x,offset()+d); 
		}

	   public final static String sgetY(String o) {
		    if (o== null) o = "0"; 
		   	return sgetY(Integer.valueOf(o)) ;
	   }
		
	   
		public final static String sgetY(Integer o) {
			log( false, "sgetY:"+String.valueOf(o)+" y = "+String.valueOf(y));
			if (o == 0) return y ;
			if (o == 9999) y = "0";
			else y = strechY(o);
			log( false, "-->"+String.valueOf(y)) ;
			return y ;
		}

	   public final static String sgetYWithOffset(String o, Integer s) {
		    if (o== null) o = "0"; 
		   	return sgetYWithOffset(Integer.valueOf(o),s) ;
	   }

		public final static String sgetYWithOffset(Integer o,Integer v) {
			if (o == 0) return valueOf(y,v) ;
			if (o == 9999) y = "0";
			else y = valueOf(strechY(o),v);
			return valueOf(y,v) ;
		}

	   public final static String sgetYAdd(String o) {
		    if (o== null) o = "0"; 
		   	return sgetYAdd(Integer.valueOf(o)) ;
	   }
		
		public final static String sgetYAdd(Integer o) {
			if (o == 0) return y ;
			y = String.valueOf(y+o*strechY());
			return y; 
		}

	   public final static String sgetWidth(String o) {
		    if (o== null) o = "0"; 
		   	return sgetWidth(Integer.valueOf(o)) ;
	   }

	   public final static String sgetWidth(String o, String d) {
		    if (o== null) o = "0"; 
		    if (d== null) d = "0"; 
		   	return sgetWidth(Integer.valueOf(o)+Integer.valueOf(d)) ;
	   }

		
		public final static String sgetWidth(Integer o) {
			if (o == 0) return width ;
			if (o == 9999) width = "0";
			else width = String.valueOf((int)(o*strech()));
			return width ;
		}

	   public final static String sgetHeight(String o) {
		    if (o== null) o = "0"; 
		   	return sgetHeight(Integer.valueOf(o)) ;
	   }

	   public final static String sgetHeight(String o,String d) {
		    if (o== null) o = "0"; 
		    if (o== null) o = "0"; 
		   	return sgetHeight(Integer.valueOf(o)+Integer.valueOf(d)) ;
	   }

		public final static String sgetHeight(Integer o) {
			if (o == 0) return height ;
			if (o == 9999) height = "0";
			else height = String.valueOf((int)(o*strechY()));
			return height ;
		}

		  public final static String containsClassWithValue(Control c, String cl, String par) {
			    log( false, "Check for : "+cl+" "+c.getClass().getName() + ">>"+packageName+"ButtonImpl");
			   	try {
		   			if (c.getClass().equals(Class.forName(packageName+"ButtonImpl"))) { 
		   				if (cl.equals("Action")) {
		   					Action a = ((Button)c).getAction() ;
		   					if (a != null) {
			   					if (par.equals("twitter")) {
			   						if (a.isTwitter()) { 
			   							log( false, "Found twitter for : "+a.getAction());
			   							return _YES ;
			   						}
			   					}
			   					if (par.equals("facebook")) {
			   						if (a.isFacebook()) { 
			   							log( false, "Found facebook for : "+a.getAction());
			   							return _YES ;
			   						}
			   					}
			   					if (par.equals("userexit")) {
			   						if (a.getUserexit() != null) { 
			   							log( false, "Found userexit for : "+a.getAction());
			   							return _YES ;
			   						}
			   					}
		   					}
		   				}
		   				return _NO ;
		   			}
		   			
	   				if (c.getClass().equals(MoviePlayerImpl.class)) {
	   					if (cl.equals("MoviePlayer")) {
		   					MoviePlayer p = (MoviePlayer)c ;
		   					
		   					if (p.getFile() != null) {
		   						if (par.equals("audio")) {
		   							return isAudio(p.getFile());
		   						}
		   					}
	   					}
	   				}

		   			
			   		if (c.getClass().equals(TableImpl.class)) {
			   			TableImpl t = (TableImpl)c ;
			   			if (t != null) {
			   				if (t.basicGetRowStructure() != null)
			   					if (t.basicGetRowStructure().getControls() != null) {
			   						return containsClassWithValue(t.basicGetRowStructure().getControls(),cl,par) ;
			   					}
			   				
			   			} 
			   			return _NO ;
			   		} else {
			   			if (c.getClass().equals(GalleryImpl.class)) {
			   				GalleryImpl g = (GalleryImpl)c ;
				   			if (g != null) {
				   				if (g.basicGetRowStructure() != null)
				   					if (g.basicGetRowStructure().getControls() != null) {
				   						return containsClassWithValue(g.basicGetRowStructure().getControls(),cl,par) ;
				   					}
				   				
				   			} 
				   			return _NO ;
				   		} 
			   		}
				} catch (ClassNotFoundException e) {
					return _NO ;
				}
			   	if (c.getClass().equals(Spacer.class)) { 
			   		String contains = _NO ;
			   		if (((Spacer)c).getTabletLayout() != null) {
			   			contains = containsClassWithValue(((Spacer)c).getTabletLayout().getControls(),cl,par) ;
			   			if (contains.equals(_YES)) return _YES ;
			   		}
			   		
			   		return containsClassWithValue(((Spacer)c).getControls(),cl,par) ;
			   	}
			   	
				return _NO;
		}
		
		  public final static String containsClass(Control c, String cl) {
			    log( false, "Check for : "+cl+" "+c.getClass().getName());
			   	try {
		   			if (c.getClass().equals(Class.forName(packageName+cl+"Impl"))) return _YES;
			   		if (c.getClass().equals(TableImpl.class)) {
			   			TableImpl t = (TableImpl)c ;
			   			if (t != null) {
			   				if (t.basicGetRowStructure() != null)
			   					if (t.basicGetRowStructure().getControls() != null) {
			   						return containsClass(t.basicGetRowStructure().getControls(),cl) ;
			   					}
			   				
			   			} 
			   			return _NO ;
			   		} else {
			   			if (c.getClass().equals(GalleryImpl.class)) {
			   				GalleryImpl g = (GalleryImpl)c ;
				   			if (g != null) {
				   				if (g.basicGetRowStructure() != null)
				   					if (g.basicGetRowStructure().getControls() != null) {
				   						return containsClass(g.basicGetRowStructure().getControls(),cl) ;
				   					}
				   				
				   			} 
				   			return _NO ;
				   		} 
			   		}
				} catch (ClassNotFoundException e) {
					return _NO ;
				}
			   	if (c.getClass().equals(Spacer.class)) { 
			   		String contains = _NO ;
			   		if (((Spacer)c).getTabletLayout() != null) {
			   			contains = containsClass(((Spacer)c).getTabletLayout().getControls(),cl) ;
			   			if (contains.equals(_YES)) return _YES ;
			   		}
			   		
			   		return containsClass(((Spacer)c).getControls(),cl) ;
			   	}
			   	
				return _NO;
		}

		public final static String containsS3(Control c) {
		    log( false, "Check for S3: "+c.getClass().getName());
		   	if (c.getClass().equals(ImageImpl.class)) { 
		   		Image u = (Image)c ;
		   		if ((u.getBucketRef() != null) && (u.getNameRef() != null)) { 
				    log( false, "found : "+c.getClass().getName());
		   			return _YES;
		   		}
		   	}
		   	if (c.getClass().equals(ButtonImpl.class)) {
		   		Button b = (Button)c;
		   		
		   		Action a = b.getAction() ;
		   		if (a != null) {
			   		if ((a.getBucketRef() != null) && (a.getNameRef() != null)) {
			   			return _YES ;
			   		}
		   		}
		   	}
		   	if (c.getClass().equals(TableImpl.class)) { 
		   		Table u = (Table)c ;
		   		String contains = _NO ;
		   		if (u.getRowStructure() != null) {
		   			contains = containsS3(u.getRowStructure().getControls()) ;
		   			if (contains.equals(_YES)) return _YES ;
		   		}
		   	}

		   	if (c.getClass().equals(Spacer.class)) {
		   		String contains = _NO ;
		   		if (((Spacer)c).getTabletLayout() != null) {
		   			contains = containsS3(((Spacer)c).getTabletLayout().getControls()) ;
		   			if (contains.equals(_YES)) return _YES ;
		   		}
		   		return containsS3(((Spacer)c).getControls()) ;
		   	}
			return _NO;
		   }

		public final static String containsMap(Control c) {
			    log( false, "Check for map: "+c.getClass().getName());
			   	if (c.getClass().equals(MapImpl.class)) return _YES;
			   	if (c.getClass().equals(Spacer.class)) {
			   		String contains = _NO ;
			   		if (((Spacer)c).getTabletLayout() != null) {
			   			contains = containsMap(((Spacer)c).getTabletLayout().getControls()) ;
			   			if (contains.equals(_YES)) return _YES ;
			   		}
			   		return containsMap(((Spacer)c).getControls()) ;
			   	}
				return _NO;
			   }

		  public final static String containsGraph(Control c) {
			    log( false, "Check for graph: "+c.getClass().getName());
			   	if (c.getClass().equals(GraphImpl.class)) return _YES;
			   	if (c.getClass().equals(Spacer.class)) { 
			   		String contains = _NO ;
			   		if (((Spacer)c).getTabletLayout() != null) {
			   			contains = containsGraph(((Spacer)c).getTabletLayout().getControls()) ;
			   			if (contains.equals(_YES)) return _YES ;
			   		}			   		
			   		return containsGraph(((Spacer)c).getControls()) ;
			   	}
			   	
				return _NO;
			   }

	   public final static String containsTable(Control c) {
		    log( false, "Check for table: "+c.getClass().getName());
		   	if (c.getClass().equals(TableImpl.class)) return _YES;
		   	if (c.getClass().equals(Spacer.class)) { 
		   		String contains = _NO ;
		   		if (((Spacer)c).getTabletLayout() != null) {
		   			contains = containsTable(((Spacer)c).getTabletLayout().getControls()) ;
		   			if (contains.equals(_YES)) return _YES ;
		   		}			   		
		   		return containsTable(((Spacer)c).getControls()) ;
		   	}
		   	
			return _NO;
		   }

	   public final static String containsTableWithMenu(Control c) {
		    log( false, "Check for table: "+c.getClass().getName());
		   	if (c.getClass().equals(TableImpl.class)) {
		   		Table t = (Table)c ;
		   		if (t.isIsMenu()) return _YES;
		   	}
		   	if (c.getClass().equals(Spacer.class)) { 
		   		String contains = _NO ;
		   		if (((Spacer)c).getTabletLayout() != null) {
		   			contains = containsTableWithMenu(((Spacer)c).getTabletLayout().getControls()) ;
		   			if (contains.equals(_YES)) return _YES ;
		   		}			   		
		   		
		   		return containsTableWithMenu(((Spacer)c).getControls()) ;
		   	}
		   	
			return _NO;
		   }

	   public final static String containsApptentive(Control c) {
		    log( false, "Check for apptentive: "+c.getClass().getName());
		   	if (c.getClass().equals(ButtonImpl.class)) { 
		   		Button b = (Button)c ;
		   		if (b.getAction() != null) {
		   			String aname = b.getAction().getAction() ; 
		   			if (aname.contains("ATFeedback")) return _YES;
		   			if (aname.contains("ATRating")) return _YES;
		   		}
		   	}
		   	if (c.getClass().equals(Spacer.class)) { 
		   		String contains = _NO ;
		   		if (((Spacer)c).getTabletLayout() != null) {
		   			contains = containsApptentive(((Spacer)c).getTabletLayout().getControls()) ;
		   			if (contains.equals(_YES)) return _YES ;
		   		}			   		
		   		
		   		return containsApptentive(((Spacer)c).getControls()) ;
		   	}
		   	
			return _NO;
		}

	   public final static String containsShareKit(Control c) {
		    log( false, "Check for ShareKit: "+c.getClass().getName());
		   	if (c.getClass().equals(ButtonImpl.class)) { 
		   		Button b = (Button)c ;
		   		if (b.getAction() != null) {
		   			Text share = b.getAction().getInformationToShare() ; 
		   			if (share != null) { 
		   				if (b.getAction().isTwitter()) return _NO ; 
		   				return _YES ;
		   			}
		   		}
		   	}
		   	if (c.getClass().equals(Spacer.class)) { 
		   		String contains = _NO ;
		   		if (((Spacer)c).getTabletLayout() != null) {
		   			contains = containsShareKit(((Spacer)c).getTabletLayout().getControls()) ;
		   			if (contains.equals(_YES)) return _YES ;
		   		}			   		
		   		
		   		return containsShareKit(((Spacer)c).getControls()) ;
		   	}
		   	
			return _NO;
		}
	   
	   public final static String containsTableWithRow(Control c) {
		    log( false, "Check for table: "+c.getClass().getName());
		   	if (c.getClass().equals(TableImpl.class)) {
		   		Table t = (Table)c ;
		   		if (t != null) { 
		   			if (t.getRowStructure() != null) log( false, "Structure: "+t.getRowStructure().toString());
				
		   			if (t.getRowStructure() != null) return _YES ;
		   		}
		   	}
		   	if (c.getClass().equals(Spacer.class)) { 
		   		String contains = _NO ;
		   		if (((Spacer)c).getTabletLayout() != null) {
		   			contains = containsTableWithRow(((Spacer)c).getTabletLayout().getControls()) ;
		   			if (contains.equals(_YES)) return _YES ;
		   		}			   		
		   		
		   		return containsTableWithRow(((Spacer)c).getControls()) ;
		   	}
		   	
			return _NO;
		   }

	   
	   public final static String containsPicker(Control c) {
		    //log( false, "Check for picker: "+c.getClass().getName());
		   	if (c.getClass().equals(PickerImpl.class)) return _YES;
		   	if (c.getClass().equals(Spacer.class)) { 
		   		String contains = _NO ;
		   		if (((Spacer)c).getTabletLayout() != null) {
		   			contains = containsPicker(((Spacer)c).getTabletLayout().getControls()) ;
		   			if (contains.equals(_YES)) return _YES ;
		   		}			   		
		   		
		   		return containsPicker(((Spacer)c).getControls()) ;
		   	}
		   	
			return _NO;
		   }
	
	   public final static String containsDatePicker(Control c) {
		    //log( false, "Class: "+c.getClass().getName());
		   	if (c.getClass().equals(DatePickerImpl.class)) return _YES;
		   	if (c.getClass().equals(Spacer.class)) { 
		   		String contains = _NO ;
		   		if (((Spacer)c).getTabletLayout() != null) {
		   			contains = containsDatePicker(((Spacer)c).getTabletLayout().getControls()) ;
		   			if (contains.equals(_YES)) return _YES ;
		   		}			   		
		   		
		   		return containsDatePicker(((Spacer)c).getControls()) ;
		   	}
		   	
			return _NO;
		   }

	   public final static String containsJoin(Property c) {
		    //log( false, "Class: "+c.getClass().getName());
		   DataType pType = c.getType() ;
		   	if (pType.getClass().equals(EntityImpl.class)) return _YES;		   	
			return _NO;
		   }

	   public final static String logInfo(Model m) {
		   log(true, "Model ---------------");
		   log(true, m.getPackage().getPackage()) ;
		   log(true, m.getMains().getName());
		   Date d = new Date() ;
		   log (true, d.toString()) ;
		   log(true, "---------------------");
		   return _NO;
	   }


	   public final static String hasTwitter(Model m) {
		   log(false, "hasConnection");
		   List<Spacer> l = m.getControls() ;
		   Iterator<Spacer> iterator = l.iterator() ; 
		   while (iterator.hasNext()) {
			   Spacer c = iterator.next() ;
			   if (containsClassWithValue(c.getControls(),"Action", "twitter").equals(_YES)) return _YES ;
			   if (containsClassWithValue(c.getControls(),"Action", "facebook").equals(_YES)) return _YES ;
		   }
		   return _NO;
	   }

	   public final static String hasConnections(Model m) {
		   log(false, "hasConnection");
		   List<Connection> l = m.getConnections() ;
		   Iterator<Connection> iterator = l.iterator() ; 
		   while (iterator.hasNext()) {
			   Connection c = iterator.next() ;
			   List<Operation> ops = c.getOperations() ;
			   log(false,c.getName()) ;
			   log(false,String.valueOf(ops.size())) ;
			   if (ops != null) {
				   if (ops.size()>0) return _YES;
			   }
		   }
		   return _NO;
	   }

	   public final static String hasTCPConnections(Model m) {
		   log(false, "hasConnection");
		   List<Connection> l = m.getConnections() ;
		   Iterator<Connection> iterator = l.iterator() ; 
		   while (iterator.hasNext()) {
			   Connection c = iterator.next() ;
			   List<Operation> ops = c.getOperations() ;
			   log(false,c.getName()) ;
			   log(false,String.valueOf(ops.size())) ;
			   if (ops != null) {
				   for (Operation o : ops) {
					   if (o.getVerb() == VERB.get("COMMAND")) return _YES;
				   }
			   }
		   }
		   return _NO;
	   }

	   
	   public final static String containsJoin(EList<Property> properties) {
		   
		   	for(Iterator<Property> i = properties.iterator() ; i.hasNext() ;  ) {
		   		Property c = i.next() ;
		   		if (containsJoin(c).equals(_YES)) { 
		   			log( false, "found join:"+c.toString());
		   			return _YES; 
		   		}
		   	}
		   	log( false, "No table found");
			return _NO;
		   }

	   public final static String containsClassWithValue(EList<Control> controls, String cl, String par) {

		    boolean logout = false ; //(cl.equals("Gallery")) ;
		    log (logout,">>> This list has : " + String.valueOf(controls.size())) ;
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		log (logout,"This control is a : "+c.getClass().getName());
		   		if (containsClassWithValue(c,cl,par).equals(_YES)) { 
		   			log( logout, "found :"+cl+ " "+c.toString());
		   			return _YES; 
		   		} else {
		   			log( logout, "could not find :"+cl+ " "+c.toString());		   			
		   		}
		   	}
		   	log( logout, ">>> No "+cl+" found");
			return _NO;
	   }
	   
	   public final static String containsClass(EList<Control> controls, String cl) {
		    boolean logout = false ; //(cl.equals("Gallery")) ;
		    log (logout,">>> This list has : " + String.valueOf(controls.size())) ;
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		log (logout,"This control is a : "+c.getClass().getName());
		   		if (containsClass(c,cl).equals(_YES)) { 
		   			log( logout, "found :"+cl+ " "+c.toString());
		   			return _YES; 
		   		} else {
		   			log( logout, "could not find :"+cl+ " "+c.toString());		   			
		   		}
		   	}
		   	log( logout, ">>> No "+cl+" found");
			return _NO;
		   }


	   public final static String containsS3(EList<Control> controls) {
		   
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		if (containsS3(c).equals(_YES)) { 
		   			log( false, "found S3:"+c.toString());
		   			return _YES; 
		   		}
		   	}
		   	log( false, "No map found");
			return _NO;
		   }

	   
	   public final static String containsMap(EList<Control> controls) {
		   
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		if (containsMap(c).equals(_YES)) { 
		   			log( false, "found map:"+c.toString());
		   			return _YES; 
		   		}
		   	}
		   	log( false, "No map found");
			return _NO;
		   }

	   
	   public final static String containsGraph(EList<Control> controls) {
		   
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		if (containsGraph(c).equals(_YES)) { 
		   			log( false, "found graph:"+c.toString());
		   			return _YES; 
		   		}
		   	}
		   	log( false, "No graph found");
			return _NO;
		   }

	   public final static String containsTable(EList<Control> controls) {
		   
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		if (containsTable(c).equals(_YES)) { 
		   			log( false, "found table:"+c.toString());
		   			return _YES; 
		   		}
		   	}
		   	log( false, "No table found");
			return _NO;
		   }

	   public final static String containsApptentive(EList<Control> controls) {
		   
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		if (containsApptentive(c).equals(_YES)) { 
		   			log( false, "found apptentive:"+c.toString());
		   			return _YES; 
		   		}
		   	}
		   	log( false, "No apptentive found");
			return _NO;
		   }

	   public final static String containsShareKit(EList<Control> controls) {
		   
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		if (containsShareKit(c).equals(_YES)) { 
		   			log( false, "found sharekit:"+c.toString());
		   			return _YES; 
		   		}
		   	}
		   	log( false, "No sharekit found");
			return _NO;
		   }
	   
	   public final static String containsTableWithMenu(EList<Control> controls) {
		   
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		if (containsTableWithMenu(c).equals(_YES)) { 
		   			log( false, "found table:"+c.toString());
		   			return _YES; 
		   		}
		   	}
		   	log( false, "No table found");
			return _NO;
		   }


	   public final static String containsTableWithRow(EList<Control> controls) {
		   
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		if (containsTableWithRow(c).equals(_YES)) { 
		   			log( false, "found table:"+c.toString());
		   			return _YES; 
		   		}
		   	}
		   	log( false, "No table found");
			return _NO;
		   }

	   
		public final static String containsDatePicker(EList<Control> controls) {
			   
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
		   		if (containsDatePicker(c).equals(_YES)) return _YES; 
		   	}
			return _NO;
		   }

		public final static String containsPicker(EList<Control> controls) {
			int j = 0;
		   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
		   		Control c = i.next() ;
			    //log( false, "Found control: "+String.valueOf(j++)+" "+c.getClass().getName());

		   		if (containsPicker(c).equals(_YES)) return _YES; 
		   	}
			return _NO;
		   }

		public final static String checkLayoutsForDatePicker(EList<LayoutRef> layouts) {
			   
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsDatePicker(c.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
		   		}
		   		
		   		if (containsDatePicker(c.getLayout().getControls()).equals(_YES)) return _YES ;
		   	}
			return _NO;
		   }

		public final static String checkLayoutsForPicker(EList<LayoutRef> layouts) {
			   
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsPicker(c.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
		   		}
		   		
		   		if (containsPicker(c.getLayout().getControls()).equals(_YES)) return _YES ;
		   	}
			return _NO;
		   }

		public final static String checkLayoutsForTable(EList<LayoutRef> layouts) {
			   
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsTable(c.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
		   		}
		   	
		   		if (containsTable(c.getLayout().getControls()).equals(_YES)) return _YES ;
		   	}
			return _NO;
		   }

		   public final static String checkLayoutsForApptentive(Model m) {
			   
				   List<View> l = m.getViews() ;
				   Iterator<View> iterator = l.iterator() ; 
				   while (iterator.hasNext()) {
					   View c = iterator.next() ;
					   List<LayoutRef> layouts = c.getLayouts() ;

					   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
					   		LayoutRef d = i.next() ;
					   		if (d.getLayout().getTabletLayout() != null) {
					   			if (containsApptentive(d.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
					   		}
					   		
					   		if (containsApptentive(d.getLayout().getControls()).equals(_YES)) return _YES ;
					   	}
			   
				   }
				   return _NO;
			   }

		   public final static String checkLayoutsForShareKit(Model m) {
			   
			   List<View> l = m.getViews() ;
			   Iterator<View> iterator = l.iterator() ; 
			   while (iterator.hasNext()) {
				   View c = iterator.next() ;
				   List<LayoutRef> layouts = c.getLayouts() ;

				   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
				   		LayoutRef d = i.next() ;
				   		if (d.getLayout().getTabletLayout() != null) {
				   			if (containsShareKit(d.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
				   		}
				   		
				   		if (containsShareKit(d.getLayout().getControls()).equals(_YES)) return _YES ;
				   	}
		   
			   }
			   return _NO;
		   }

		
		public final static String checkLayoutsForTableWithMenu(EList<LayoutRef> layouts) {
			   
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsTableWithMenu(c.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
		   		}
		   		
		   		if (containsTableWithMenu(c.getLayout().getControls()).equals(_YES)) return _YES ;
		   	}
			return _NO;
		   }

		public final static String checkLayoutsForTableWithRow(EList<LayoutRef> layouts) {
			   
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsTableWithRow(c.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
		   		}
		   		
		   		if (containsTableWithRow(c.getLayout().getControls()).equals(_YES)) return _YES ;
		   	}
			return _NO;
		   }

		
		public final static String checkLayoutsForGraph(EList<LayoutRef> layouts) {
			   
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsGraph(c.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
		   		}
		   		
		   		if (containsGraph(c.getLayout().getControls()).equals(_YES)) return _YES;
		   	}
			return _NO;
		   }

		public final static String checkLayoutsForS3(EList<LayoutRef> layouts) {
			   
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsS3(c.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
		   		}
		   		if (containsS3(c.getLayout().getControls()).equals(_YES)) return _YES;
		   	}
			return _NO;
		   }

		
		public final static String checkLayoutsForMap(EList<LayoutRef> layouts) {
			   
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsMap(c.getLayout().getTabletLayout().getControls()).equals(_YES)) return _YES;
		   		}
		   		if (containsMap(c.getLayout().getControls()).equals(_YES)) return _YES;
		   	}
			return _NO;
		   }

		public final static String checkLayoutsForClassWithValue(EList<LayoutRef> layouts,String cl, String par) {
			   
			
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsClassWithValue(c.getLayout().getTabletLayout().getControls(),cl, par).equals(_YES)) return _YES;
		   		}
		   		
		   		if (containsClassWithValue(c.getLayout().getControls(),cl,par).equals(_YES)) return _YES ;
		   	}
			return _NO;
		   }

		public final static String checkLayoutsForClass(EList<LayoutRef> layouts,String cl) {
			   
			
		   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
		   		LayoutRef c = i.next() ;
		   		if (c.getLayout().getTabletLayout() != null) {
		   			if (containsClass(c.getLayout().getTabletLayout().getControls(),cl).equals(_YES)) return _YES;
		   		}
		   		
		   		if (containsClass(c.getLayout().getControls(),cl).equals(_YES)) return _YES ;
		   	}
			return _NO;
		   }

		
		public final static String firstLowerCase(String o) {
			return o.substring(0,1).toLowerCase()+o.substring(1);
		}

		public final static String firstUpperCase(String o) {
			return o.substring(0,1).toUpperCase()+o.substring(1);
		}
		
		public final static String allLowerCase(String o) {
			return o.toLowerCase() ;
		}

		
		public final static String counter(String o) {
			if (counter<9) return o+String.valueOf(counter++);
			return String.valueOf(counter++);
		}

		public final static String counter(String o, Integer i) {
			if (counter<9) return o+String.valueOf(i+counter++);
			return String.valueOf(i+counter++);
		}

		public final static String counterNoInc(String o) {
			if (counter<9) return o+String.valueOf(counter);
			return String.valueOf(counter);
		}

		public final static String counterNoInc(String o, Integer i) {
			if (counter<9) return o+String.valueOf(i+counter);
			return String.valueOf(i+counter);
		}

		public final static String restartCounter(String o) {
			if ((o == null) | (o == "")) counter = 0;
			else counter = Integer.valueOf(o);
			return String.valueOf(counter);
		}

		public final static String prepareForBreak(String o) {
			counter = 0;
			return _NO;
		}

		public final static String breakDone(String o) {
			if (counter == 0) return _NO ;
			else return _YES;
		}

		public final static String doBreak(String o) {
			return String.valueOf(counter++);
		}

		public final static String setReadOnly(String owner) {
			if (readOnly.equals(_NO)) {
				readOnly = _YES ;
				roOwner = owner ;
			}
			return _YES;
		}

		public final static String resetReadOnly(String owner) {
			if (owner.equals(roOwner)) { readOnly = _NO; roOwner = "";}
			return readOnly;
		}

		public final static String getReadOnly(String owner) {
			
			return readOnly;
		}
		
		public final static String standardButtonIcon(StandardImage icon) {
			String iconName = icon.getLiteral();
			if (platform == Platforms.SYMBIAN360x640) {
				if (iconName.equals("DetailDisclosure")) iconName = "arrow-icon.png" ;
				if (iconName.equals("ContactAdd")) iconName = "contact-add.png" ;
			}
			if ((platform == Platforms.android320x480) | (platform == Platforms.android480x854) | (platform == Platforms.android480x800)) {
	
			}
			return iconName ;
		}
		
		public final static String getPlotName(PlotDefinition p) {
			return p.getName();
		}

		public final static String replace(String source, String from, String to) {
			
			return source.replace(from.charAt(0), to.charAt(0)) ;
			
		}

		public final static String convertAmpercent(String source) {
			if ((source == null) | (source=="")) return source ;
			return source.replace("&", "&amp;") ;
		}
		
		public final static String resourcify(String source) {
			if ((source == null) | (source=="")) return "v"+String.valueOf(System.currentTimeMillis());
			
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String replacements[] = {"!", "*", "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]", "  "};
			for (int i = 0 ; i < replacements.length; i++) 
			{
				source = source.replace(replacements[i], "") ;
			}
			source = source.replace(" ", "_") ;
			source = source.replace("-", "_") ;
			source = source.replace(".", "_") ;
			if (source.length()>1) { 
				if (source.lastIndexOf("_") ==  source.length()-1) return source.substring(0, source.length()-1) ;
			} else {
				source = "v"+String.valueOf(System.currentTimeMillis()); 
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return source ;
		}

		public final static String directoryForPackage(Model m) {
			String source = m.getPackage().getPackage() ;
			return source.replace(".", "/") ;
		}

		public final static String textBorder(TextBorder b) {
			if (b.getName().equals("bezel")) return "Bezel" ;
			if (b.getName().equals("none"))  return "None" ;
			if (b.getName().equals("rounded")) return "RoundedRect" ;
			if (b.getName().equals("line")) return "Line" ;
			return "None" ;
		}
		
		public final static String getVAlignment(Alignment a) {
			log(false,"VAlignment:"+a.getName()) ;
			if (isAndroid())  {
				if (a.getName().equals("Fill")) return "fill_vertical" ;
				if (a.getName().equals("Top")) return "top" ;
				if (a.getName().equals("Bottom")) return "bottom" ;	
				return "center_vertical" ;
			} else {
			if (a.getName().equals("Fill")) return "Fill" ;
			if (a.getName().equals("Top")) return "Top" ;
			if (a.getName().equals("Bottom")) return "Bottom" ;
			}
			return "Center" ; 
		}

		public final static String getHAlignment(Alignment a) {
			log(false,"HAlignment:"+a.getName()) ;
			if (isAndroid())  {
				if (a.getName().equals("Left")) return "left" ;
				if (a.getName().equals("Center")) return "center_horizontal" ;
				if (a.getName().equals("Right")) return "right" ;	
				return "left" ;
			} else {
			if (a.getName().equals("Left")) return "Left" ;
			if (a.getName().equals("Center")) return "Center" ;
			if (a.getName().equals("Right")) return "Right" ;
			}
			return "Left" ; 
		}

		public final static String getKeyboardType(TextEntry t) {
			log( false, ">> TextEntry"+t.toString());
			if (t.getName().equals("numberEntry")) return "NumberPad" ;
			if (t.getName().equals("phoneEntry")) return "PhonePad" ;
			if (t.getName().equals("emailEntry")) return "EmailAddress" ;
			if (t.getName().equals("decimalEntry")) return "DecimalPad" ;
			if (t.getName().equals("urlEntry")) return "URL" ;
			if (t.getName().equals("asciiEntry")) return "ASCIICapable" ;
			if (t.getName().equals("textEntry")) return "Alphabet" ;
			return "Alphabet" ; 
		}

		public final static String getButtonStyle(ButtonStyle a) {
			if (a.getName().equals("Bordered")) return "Bordered" ;
			if (a.getName().equals("Gradient")) return "Gradient" ;
			if (a.getName().equals("Custom")) return "Custom" ;
			if (a.getName().equals("Navbar")) return "Navbar" ;
			return "Bordered" ; 
		}

		public final static String getMenuItemStyle(MenuItemStandardButton a) {
			if (a.getName().equals("checkmark")) return "Checkmark" ;
			if (a.getName().equals("indicator")) return "DisclosureIndicator" ;
			if (a.getName().equals("detail")) return "DetailDisclosureButton" ;
			return "None" ; 
		}
		
		public final static Operation operationForName(Connection c, String s) {
			Operation o = null;
			EList<Operation> ops = c.getOperations() ;
			for (int i = 0 ; i < ops.size() ; i++) {
				o = ops.get(i) ;
				if (o.getName().equals(s)) return o ;
			}
			return ops.get(0) ;
		}

		public final static String getVerb(VERB a) {
			if (a.getName().equals("POST")) return "POST" ;
			if (a.getName().equals("PUT")) return "PUT" ;
			if (a.getName().equals("DELETE")) return "DELETE" ;
			if (a.getName().equals("HEAD")) return "HEAD" ;
			if (a.getName().equals("COMMAND")) return "COMMAND" ;
			if (a.getName().equals("CLOSE")) return "CLOSE" ;
			return "GET" ; 
		}

		
		public final static String getProtocol(Protocol a) {
			if (a.getName().equals("SOAP")) return "SOAP" ;
			if (a.getName().equals("ATOM")) return "ATOM" ;
			if (a.getName().equals("RSS")) return "RSS" ;
			if (a.getName().equals("TCP")) return "TCP" ;
			return "HTTP" ; 
		}

		public final static String getProtocolVersion(ProtocolVersion a) {
			if (a.getName().equals("soap12")) return "soap12" ;
			if (a.getName().equals("v12")) return "soap12" ;
			if (a.getName().equals("soap11")) return "soap11" ;
			if (a.getName().equals("v11")) return "soap11" ;
			if (a.getName().equals("rss2")) return "rss2" ;
			if (a.getName().equals("rss1")) return "rss1" ;
			if (a.getName().equals("v2")) return "rss2" ;
			if (a.getName().equals("v1")) return "rss1" ;
			return "None" ; 
		}

		
		   public final static String menuTabWidth(EList<View> views) {
			    int n = views.size() ;
			    log(false,"# of views:"+String.valueOf(n)) ;
			    log(false,"Platform:"+String.valueOf(platform)) ;
			    
			   	if (n>0) {
			   		int size = 80 ;
					if (platform == Platforms.android320x480) {
						size = 320/n ;
					}
					if (platform == Platforms.android480x800) {
						size = 480/n ;
					}
					if (platform == Platforms.android480x854) {
						size = 480/n ;
					}
					if (size < 40) size = 40 ;
					return String.valueOf(size) ;
			   	}
			   
			   return "80" ;
			   
		   }

		   public final static String convertToInt(String s) {
			   if (s != null) {
				   Float f = new Float(s) ;
				   return String.valueOf(f.intValue()) ;
			   }
			   return "0" ;
		   }

		   public final static String convertToDirectoryStructure(String s) {
			   if (s != null) {
				  return s.replace('.', '/') ;
			   }
			   return "" ;
		   }

		   public final static Boolean isAndroid() {
		   
			   log(false,"Plaform:"+String.valueOf(platform));
			   
			   if (platform == Platforms.android1024x600) {
				   return true ;
			   }

			   if (platform == Platforms.android320x480) {
				   return true ;
			   }

			   if (platform == Platforms.android480x800) {
				   return true ;
			   }

			   if (platform == Platforms.android480x854) {
				   return true ;
			   }

			   return false ;
		   }
		   
		   public final static String hasAddEventAction(Control c) {
			   	try {
			   		if (c.getClass().equals(TableImpl.class)) {
			   			TableImpl t = (TableImpl)c ;
			   			if (t != null) {
			   				if (t.basicGetRowStructure() != null)
			   					if (t.basicGetRowStructure().getControls() != null) {
			   						return hasAddEventAction(t.basicGetRowStructure().getControls()) ;
			   					}
			   				
			   			} 
			   			return _NO ;
			   		} else {
			   			if (c.getClass().equals(Class.forName(packageName+"DatePickerImpl"))) {
			   				DatePicker d = (DatePicker)c ;
			   				if (d.getAction() != null) {
			   					
			   					if(d.getAction().getAction() != null) {
				   					if (d.getAction().getAction().equals("addEvent")) {
				   					return _YES;
				   					} 
				   				}
			   				}
			   				return _NO;
			   				
			   			}
			   		}
				} catch (ClassNotFoundException e) {
					return _NO ;
				}
			   	if (c.getClass().equals(Spacer.class)) return hasAddEventAction(((Spacer)c).getControls()) ;

			   return _NO ;
		   }
		   
		   
		   public final static String hasAddEventAction(EList<Control> controls) {
			       
			   	for(Iterator<Control> i = controls.iterator() ; i.hasNext() ;  ) {
			   		Control c = i.next() ;
			   		
			   		if (hasAddEventAction(c).equals(_YES)) { 
			   			
			   			return _YES; 
			   		}
			   	}
			   
			   
			   return _NO ;
		   }
		   
			public final static String checkLayoutsForAddEventAction(EList<LayoutRef> layouts) {
				   
				
			   	for(Iterator<LayoutRef> i = layouts.iterator() ; i.hasNext() ;  ) {
			   		LayoutRef c = i.next() ;
			   		
			   		if (hasAddEventAction(c.getLayout().getControls()).equals(_YES)) return _YES ;
			   	}
				return _NO;
			}
			
			public final static String getXcodeId(String i) {
				int u = i.length() ;
				String trail = "DDC5E12600D3760F" ;
				return "DC8855B"+i+trail.substring(u) ;
			}

			public final static String getXcodeId2(String i) { 
				int u = i.length() ;
				String trail = "DDC5E12600D3760F" ;
				return "DC8855A"+i+trail.substring(u) ;
			}

			public final static String getXcodeId3(String i) {
				int u = i.length() ;
				String trail = "DDC5E12600D3760F" ;
				return "DC8855C"+i+trail.substring(u) ;
			}

			public final static String isHTTPS(String url) {
				if (url.startsWith("https")) return _YES;
				return _NO ;
			}

			public final static String trimSingleQuotes(String o) {
				 String u = "" ;
				 if (o == null) return null ;
				 if (o.startsWith("'")) { 
					 u = o.substring(1) ; 
				 } else {
					 u = o ;
				 }
				 if (u.endsWith("'")) { 
					 return u.substring(0, u.length()-1) ;
				 }
				 return u ;
			   }

			public final static String setNavBarColor(Color c) {
				navBarColor = c ;
				return _YES ;
			}
			
			public final static Color getNavBarColor(Object o) {
				return navBarColor ;
			}
			
			public final static Object getObjectAtIndex(EList<Object> o, String u) {
				if (o == null) return null ;
				return o.get(Integer.valueOf(u)) ;
			}
			
			public final static String isIn(Object u, EList<Object> o ) {
				if (o == null) return _NO ;
				if (o.contains(u)) return _YES ;
				return _NO ;
			}

			public final static String indexOf(Object u, EList<Object> o ) {
				if (o == null) return "" ;
				if (u == null) return "" ;
				if (o.indexOf(u)>=0) return String.valueOf(o.indexOf(u)) ;
				return "" ;
			}

}
